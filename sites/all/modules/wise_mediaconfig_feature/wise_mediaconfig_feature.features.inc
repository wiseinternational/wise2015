<?php
/**
 * @file
 * wise_mediaconfig_feature.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function wise_mediaconfig_feature_image_default_styles() {
  $styles = array();

  // Exported image style: homepage_fullwidth_header.
  $styles['homepage_fullwidth_header'] = array(
    'label' => 'Homepage_fullwidth_header',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1440,
          'height' => 668,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: nodeblock.
  $styles['nodeblock'] = array(
    'label' => 'Nodeblock',
    'effects' => array(
      10 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 262,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: partners.
  $styles['partners'] = array(
    'label' => 'Partners',
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 80,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: rounded_200px.
  $styles['rounded_200px'] = array(
    'label' => 'Rounded 200px',
    'effects' => array(
      6 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 200,
        ),
        'weight' => 1,
      ),
      7 => NULL,
    ),
  );

  // Exported image style: subpage_block.
  $styles['subpage_block'] = array(
    'label' => 'Subpage block',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 160,
          'height' => 160,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vervolgpagina_fullwidth_header.
  $styles['vervolgpagina_fullwidth_header'] = array(
    'label' => 'Vervolgpagina_fullwidth_header',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1440,
          'height' => 360,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
