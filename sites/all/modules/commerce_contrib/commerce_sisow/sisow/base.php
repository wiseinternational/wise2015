<?php

require_once(drupal_get_path('module', 'commerce_sisow') . '/sisow/sisow.cls5.php');

function _settings($settings = null, $base, $payment) {
    $form = array();
    $currencies = $base . 'currencies';

    // Merge default settings into the stored settings array.
    $default_currency = variable_get('commerce_default_currency', 'EUR');

    if ($payment == 'ebill' || $payment == 'overboeking') {
        $settings = (array) $settings + array(
            'merchantid' => '',
            'merchantkey' => '',
            'currency_code' => in_array($default_currency, array_keys($currencies())) ? $default_currency : 'EUR',
            'server' => 'testmode',
            'omschrijving' => '',
            'dagen' => '',
            'include' => '',
            'pending' => '',
            'complete' => '',
            'cancel' => '',
        );
    } else {
        $settings = (array) $settings + array(
            'merchantid' => '',
            'merchantkey' => '',
            'currency_code' => in_array($default_currency, array_keys($currencies())) ? $default_currency : 'EUR',
            'server' => 'testmode',
            'omschrijving' => '',
            'pending' => '',
            'complete' => '',
            'cancel' => '',
        );
    }

    $form['merchantid'] = array(
        '#type' => 'textfield',
        '#title' => t('Sisow Merchant ID'),
        '#description' => t('The Merchant ID from your Sisow account.'),
        '#default_value' => $settings['merchantid'],
        '#required' => TRUE,
    );
    $form['merchantkey'] = array(
        '#type' => 'textfield',
        '#title' => t('Sisow Merchant Key'),
        '#description' => t('The Merchant Key from your Sisow account.'),
        '#default_value' => $settings['merchantkey'],
        '#required' => TRUE,
    );
    $form['omschrijving'] = array(
        '#type' => 'textfield',
        '#title' => t('Description'),
        '#description' => t('Description in front of the order nr.'),
        '#default_value' => $settings['omschrijving'],
        '#required' => TRUE,
    );
    $form['server'] = array(
        '#type' => 'radios',
        '#title' => t('Testmode'),
        '#options' => array(
            'testmode' => ('Testmode - use for testing'),
            'live' => ('Live - use for processing real transactions'),
        ),
        '#default_value' => $settings['server'],
    );

    if ($payment == 'ebill' || $payment == 'overboeking') {
        $form['dagen'] = array(
            '#type' => 'textfield',
            '#title' => t('Days'),
            '#description' => t('The customer recieves a reminder after ... days.'),
            '#default_value' => $settings['dagen'],
            '#required' => TRUE,
        );

        if ($payment == 'ebill') {
            $form['include'] = array(
                '#type' => 'radios',
                '#title' => t('Include iDEAL'),
                '#options' => array(
                    'true' => ('Include Bank Account information, customer also can pay through a bank transfer'),
                    'false' => ("Don't include Bank Account information"),
                ),
                '#default_value' => $settings['include'],
            );
        } else {
            $form['include'] = array(
                '#type' => 'radios',
                '#title' => t('Include iDEAL'),
                '#options' => array(
                    'true' => ('Include an iDEAL payment opportunity into an ebill'),
                    'false' => ("Don't include iDEAL"),
                ),
                '#default_value' => $settings['include'],
            );
        }
    }

    $form['omschrijving'] = array(
        '#markup' => 'The conditions for displaying a payment method can you set with the payment conditions.',
    );

    return $form;
}

function _betaling($order, $payment_method) {
    // Return an error if the enabling action's settings haven't been configured.
    if (empty($payment_method['settings']['merchantid']) || empty($payment_method['settings']['merchantkey'])) {
        drupal_set_message($payment_method['title'] . t(" isn't configured yet."), 'error');
        return array();
    }

    $settings = array(
        // Return to the previous page when payment is canceled
        'cancel_return' => url('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
        // Return to the payment redirect page for processing successful payments
        'return' => url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
        // Specify the current payment method instance ID in the notify_url
        'payment_method' => $payment_method['instance_id'],
    );

    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $amount = $wrapper->commerce_order_total->amount->value();

    $sisow = new Sisow($payment_method['settings']['merchantid'], $payment_method['settings']['merchantkey']);
    $sisow->payment = $payment_method['payment_code'];
    $sisow->amount = round($amount / 100, 2);

    $omschrijving = '';
    if (isset($payment_method['settings']['omschrijving']) && $payment_method['settings']['omschrijving'] != '') {
        $omschrijving = $payment_method['settings']['omschrijving'] . ' ';
    }

    $omschrijving .= $order->order_number;
    $sisow->description = $omschrijving;
    $sisow->testmode = 'true';
    if (isset($order->data['bankkeuze'])) {
        $sisow->issuerId = $order->data['bankkeuze'];
    }
    $sisow->purchaseId = $order->order_number;
    $sisow->notifyUrl = commerce_sisow_notify_url($settings['payment_method']);
    $sisow->cancelUrl = $settings['cancel_return'];
    $sisow->returnUrl = $sisow->notifyUrl;
    //$sisow->returnUrl = $settings['return'];

    if (($ex = $sisow->TransactionRequest(_prep($order, $payment_method))) < 0) {
        drupal_set_message(t('Betalen met ' . $payment_method['title'] . 'is nu niet mogelijk.') . ' (' . $ex . ', ' . $sisow->errorCode . ')', 'error');
        return;
    } else {
        if ($sisow->payment != 'ecare' && $sisow->payment != 'overboeking' && $sisow->payment != 'ebill') {
            drupal_goto($sisow->issuerUrl);
        } else {
            _notify($sisow->trxId, $order->order_number, $payment_method);
            if ($sisow->payment == 'overboeking' && $sisow->payment == 'ebill')
                drupal_set_message(t("Sisow b.v. process this transaction further. You'll recieve an e-mail with further instructions."));
            drupal_goto($sisow->returnUrl);
        }
    }
}

function _prep($order, $payment_method) {

    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $arg = array();

    //get billing address information
    if (!empty($order_wrapper->commerce_customer_billing->commerce_customer_address)) {
        $billing_address = $order_wrapper->commerce_customer_billing->commerce_customer_address->value();
        $arg['billing_firstname'] = $billing_address['first_name'];
        $arg['billing_lastname'] = ($billing_address['last_name'] != '') ? $billing_address['last_name'] : $billing_address['name_line'];
        $arg['billing_mail'] = $order->mail;
        $arg['billing_company'] = (isset($billing_address['organisation_name'])) ? $billing_address['organisation_name'] : '';
        $arg['billing_address1'] = $billing_address['thoroughfare'];
        $arg['billing_address2'] = $billing_address['premise'];
        $arg['billing_zip'] = $billing_address['postal_code'];
        $arg['billing_city'] = $billing_address['locality'];
        $arg['billing_country'] = '';
        $arg['billing_countrycode'] = $billing_address['country'];
        //$arg['birthdate'] = $gebdatum;
    }

    //get shipping address information
    if (module_exists('commerce_shipping')) {
        if (!empty($order_wrapper->commerce_customer_shipping->commerce_customer_address)) {
            $shipping_address = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();

            $arg['shipping_firstname'] = $shipping_address['first_name'];
            $arg['shipping_lastname'] = ($shipping_address['last_name'] != '') ? $shipping_address['last_name'] : $shipping_address['name_line'];
            $arg['shipping_mail'] = $order->mail;
            $arg['shipping_company'] = (isset($shipping_address['organisation_name'])) ? $shipping_address['organisation_name'] : '';
            $arg['shipping_address1'] = $shipping_address['thoroughfare'];
            $arg['shipping_address2'] = $shipping_address['premise'];
            $arg['shipping_zip'] = $shipping_address['postal_code'];
            $arg['shipping_city'] = $shipping_address['locality'];
            $arg['shipping_country'] = '';
            $arg['shipping_countrycode'] = $shipping_address['country'];
        }
    }

    $order_total = $order_wrapper->commerce_order_total->value();
    $arg['amount'] = $order_total['amount'];
    $arg['currency'] = $order_total['currency_code'];

    if (module_exists('commerce_tax')) {
        $taxamt = commerce_round(COMMERCE_ROUND_HALF_UP, commerce_tax_total_amount($order_total['data']['components'], TRUE, $arg['currency']));
        $taxamt += commerce_round(COMMERCE_ROUND_HALF_UP, commerce_tax_total_amount($order_total['data']['components'], FALSE, $arg['currency']));
    } else {
        $taxamt = 0;
    }
    $arg['tax'] = $taxamt;

    //testmodes
    $arg['testmode'] = ($payment_method['settings']['server'] == 'testmode') ? 'true' : 'false';

    //producten
    /*
      $productnr = 1;

      foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      $order_line = $line_item_wrapper->value();
      $product = commerce_product_load($line_item_wrapper->commerce_product->product_id->value());
      $unit_price = entity_metadata_wrapper('commerce_product', $product)->commerce_price->value();
      $product_total = $line_item_wrapper->commerce_total->value();

      $arg['product_id_'.$productnr]	 		= $product->sku;
      $arg['product_description_'.$productnr] = $product->title;
      $arg['product_quantity_'.$productnr] 	= $order_line->quantity;

      if (module_exists('commerce_tax')) {
      $taxamt = commerce_round(COMMERCE_ROUND_HALF_UP, commerce_tax_total_amount($product_total['data']['components'], TRUE, $arg['currency']));
      $taxamt += commerce_round(COMMERCE_ROUND_HALF_UP, commerce_tax_total_amount($product_total['data']['components'], FALSE, $arg['currency']));

      $taxrate = 0;
      foreach(commerce_tax_components($product_total['data']['components']) as $tax)
      $taxrate += ($tax['price']['data']['tax_rate']['rate'] * 100.0);
      }
      else {
      $taxamt = 0;
      $taxrate = 0;
      }

      $total=commerce_price_component_total($product_total);
      $nettotal=commerce_price_component_total($product_total, 'base_price');
      $arg['product_netprice_'.$productnr] 	= $unit_price['amount'];
      $arg['product_total_'.$productnr] 		= $total['amount'];
      $arg['product_nettotal_'.$productnr] 	= $nettotal['amount'];
      $arg['product_tax_'.$productnr] 		= $taxamt;
      $arg['product_taxrate_'.$productnr] 	= round($taxrate * 100.0);
      //$arg['product_weight_'.$productnr] 		= $product;

      $productnr ++;
      }
     */

    //Data needed for Sisow
    if (isset($payment_method['settings']['include']))
        $arg['including'] = $payment_method['settings']['include'];
    if (isset($payment_method['settings']['dagen']))
        $arg['days'] = $payment_method['settings']['dagen'];

    return $arg;
}

function _notify($trxid, $orderid, $payment_method) {
    $order = commerce_order_load($orderid);
	$sisow = new Sisow($payment_method['settings']['merchantid'], $payment_method['settings']['merchantkey']);
	if (($ex = $sisow->statusRequest($trxid)) < 0) {
		watchdog('commerce_sisow', 'Status Request Failed', array('@code' => $ex, '@error' => $ex), WATCHDOG_ERROR);
		return;
	}
        
	$transaction = commerce_payment_transaction_new($payment_method['method_id'], $order->order_id);
	$transaction->instance_id = $payment_method['instance_id'];

	$transaction->remote_id = $trxid;
	$transaction->amount = $order->commerce_order_total['und']['0']['amount'];
	$transaction->currency_code = $order->commerce_order_total['und']['0']['currency_code'];
	//$transaction->payload[REQUEST_TIME] = $ipn;
	// Set the transaction's statuses based on the IPN's payment_status.
	$transaction->remote_status = $sisow->status;

	//$status = '';
	// If we didn't get an approval response code...
	switch ($sisow->status) {
		case 'Failure':
			$transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
			$transaction->message = t("The payment has failed.");
			break;
		case 'Cancelled':
			$transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
			$transaction->message = t("The payment has cancelled.");
			break;
		case 'Success':
			$transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
			$transaction->message = t('The payment has completed.');
			break;
		case 'Expired':
			$transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
			$transaction->message = t('The payment has expired.');
			break;
		case 'Reservation':
			$transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
			$transaction->message = t("There has been made an reservation for Sisow ecare, the amount isn't payed yet!.");
			break;
		default:
			$transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
			$transaction->message = t('The payment status is still open');
			break;
	}
	
	/* Drupal 7 query */
	$query = db_select('commerce_sisow', 's');
	$query->condition('s.trxid', $trxid, '=')
			->condition('s.status', $transaction->status, '=')
			->fields('s', array('status'))
			->range(0, 50);
	$result = $query->execute();
	
	if($result->rowCount() == 0)
	{
		// Save the transaction information.
		commerce_payment_transaction_save($transaction);
		watchdog($payment_method['base'], 'Sisow Notify processed for Order @order_number with ID @txn_id.', array('@txn_id' => $trxid, '@order_number' => $order->order_number), WATCHDOG_INFO);
		
		$record = array(
			"trxid" => $trxid,
			"orderid" => $orderid,
			"status" => $sisow->status,
			"invoicenr" => '',
			"creditnr" => '',
			"payment" => $payment_method['title'],
		);

		drupal_write_record('commerce_sisow', $record);
	}
            
    if (!isset($_GET['notify']) && !isset($_GET['callback'])) {
        header('location: ' . url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)));
    }
    exit;
}

?>