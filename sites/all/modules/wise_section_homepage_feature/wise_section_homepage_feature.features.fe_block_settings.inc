<?php
/**
 * @file
 * wise_section_homepage_feature.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function wise_section_homepage_feature_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-past_activities-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'past_activities-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
