<?php
/**
 * @file
 * wise_section_homepage_feature.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wise_section_homepage_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
