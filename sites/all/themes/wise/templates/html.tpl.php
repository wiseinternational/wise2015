<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?>
<!DOCTYPE html>
<!-- Sorry no IE7 support! -->
<!-- @see http://foundation.zurb.com/docs/index.html#basicHTMLMarkup -->

<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:700|Vollkorn' rel='stylesheet' type='text/css'>
  
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript"> 
	
	jQuery.fn.extend({
	  scrollTo : function(speed, easing) {
	    return this.each(function() {
	      var targetOffset = jQuery(this).offset().top;
	      jQuery('html,body').animate({scrollTop: targetOffset}, speed, easing);
	    });
	  }
	});	
	
	jQuery(document).ready(function () {
		jQuery( ".page-taxonomy-term main" ).remove( "strong" );
	
		jQuery('.next-section').click(function(e){
		    e.preventDefault();
		    var $this = jQuery(this),
		        $next = $this.parent().next();
		    
		    $next.scrollTo(400, 'linear');
		});
		
		//Click event to scroll to top
		jQuery('.scrollToTop').click(function(){
			jQuery('html, body').animate({scrollTop : 0},800);
			return false;
		});		
	
		jQuery("#open-content").hide();
		jQuery(".block-custom-search-blocks-1").hide();
		jQuery("#close-search").hide();		
		
		jQuery("#open-search").click(function(){
			jQuery(".block-custom-search-blocks-1").show("fast");
			jQuery("#close-search").show("fast");					
		});
	
		jQuery("#close-search").click(function(){
			jQuery(".block-custom-search-blocks-1").hide("slow");
			jQuery("#close-search").hide("fast");			
		});	


		jQuery(".block-commerce-cart-cart .view-content").click(function(){
			jQuery(".block-commerce-cart-cart .view-footer").show("fast");
			jQuery("#close-search-2").show("fast");					
		});
	
		jQuery("#close-search-2").click(function(){
			jQuery(".block-commerce-cart-cart .view-footer").hide("slow");
			jQuery("#close-search-2").hide("fast");			
		});	

		
		if (window.location.href.indexOf('?search_api_views_fulltext_op') > -1) {
			jQuery(".section-nuclear-monitor .block-views-nuclear-monitor-search-block-1").hide();
		}
	    
	    
	});		
	
	
	jQuery(function(){ jQuery("#views-exposed-form-leverancier-search-page label, #user-login-form label, #subForm label, #finder-block-content_finder-wrapper label").inFieldLabels(); });
	
		</script>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div class="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
  <?php print _zurb_foundation_add_reveals(); ?>
  <script>
    (function ($, Drupal, window, document, undefined) {
      $(document).foundation();
    })(jQuery, Drupal, this, this.document);
    
			jQuery('.owl-carousel').owlCarousel({
			    loop:true,
			    autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,			    
					stagePadding: 50,			    
			    margin:10,
			    nav:true,
			    responsive:{
			        0:{
			            items:1
			        },
			        600:{
			            items:2	
			        },
			        1000:{
			            items:3
			        },
			        1400:{
			            items:4
			        },
			        1800:{
			            items:5
			        }
			        
			    }
			})
			
		jQuery('.node-type-petition .field-name-body').readmore({
		  speed: 75,
		  collapsedHeight: 120,
		  lessLink: '<a href="#">read less</a>'
		});			
		
	jQuery(function() {
	    jQuery('.highlight .block').matchHeight();
	});		

	jQuery(".section-map .highlighted .block-block .block-title").click(function() {
		jQuery(".section-map .highlighted .block-block .legend").slideToggle("slow");
		jQuery(this).toggleClass("active");
		return false;
	});		
	jQuery('.views-field-field-your-name .field-content:contains("MEP")').parent().parent().addClass('green-mep');
		
  </script>
</body>
</html>