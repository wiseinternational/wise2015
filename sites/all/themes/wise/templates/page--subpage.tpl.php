<!--.page -->
<div role="document" class="page">
  <!--.l-header region -->
  <header role="banner" class="l-header">
  <?php if (!empty($page['cart'])): ?>
  <?php print render($page['cart']); ?>
  <?php endif; ?>
	  
    <?php if ($top_bar): ?>
      <!--.top-bar -->
      <?php if ($top_bar_classes): ?>
      <div class="<?php print $top_bar_classes; ?>">
      <?php endif; ?>
        <nav class="top-bar"<?php print $top_bar_options; ?>>
          <ul class="title-area">
            <li class="name"><h1><?php print $linked_site_name; ?></h1></li>
            <li class="toggle-topbar menu-icon"><a href="#"><span><?php print $top_bar_menu_text; ?></span></a></li>
          </ul>
          <section class="top-bar-section">
            <?php if ($top_bar_main_menu) :?>
              <?php print $top_bar_main_menu; ?>
            <?php endif; ?>
            <?php if ($top_bar_secondary_menu) :?>
              <?php print $top_bar_secondary_menu; ?>
            <?php endif; ?>
          </section>
        </nav>
      <?php if ($top_bar_classes): ?>
      </div>
      <?php endif; ?>
      <!--/.top-bar -->
    <?php endif; ?>

    <!-- Title, slogan and menu -->
    <?php if ($alt_header): ?>
    <section class="row <?php print $alt_header_classes; ?>">

      <?php if ($linked_logo): print $linked_logo; endif; ?>

      <?php if ($site_name): ?>
        <?php if ($title): ?>
          <div id="site-name" class="element-invisible">
            <strong>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </strong>
          </div>
        <?php else: /* Use h1 when the content title is empty */ ?>
          <h1 id="site-name">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
          </h1>
        <?php endif; ?>
      <?php endif; ?>

      <?php if ($site_slogan): ?>
        <h2 title="<?php print $site_slogan; ?>" class="site-slogan"><?php print $site_slogan; ?></h2>
      <?php endif; ?>

      <?php if ($alt_main_menu): ?>
        <nav id="main-menu" class="navigation" role="navigation">
          <?php print ($alt_main_menu); ?>
        </nav> <!-- /#main-menu -->
      <?php endif; ?>

      <?php if ($alt_secondary_menu): ?>
        <nav id="secondary-menu" class="navigation" role="navigation">
          <?php print $alt_secondary_menu; ?>
        </nav> <!-- /#secondary-menu -->
      <?php endif; ?>

    </section>
    <?php endif; ?>
    <!-- End title, slogan and menu -->

    <?php if (!empty($page['header'])): ?>
      <!--.l-header-region -->
      <section class="l-header-region row">
        <div class="large-12 columns">
          <?php print render($page['header']); ?>
        </div>
      </section>
      <!--/.l-header-region -->
    <?php endif; ?>

	<div class="search">
		<div id="close-search"></div>      	
		<div id="open-search"></div>
		<?php print render($page['search_block']); ?>
	</div>
  </header>
  <!--/.l-header -->



  <?php if (!empty($page['featured_1']) || !empty($page['featured_2']) || !empty($page['featured_3'])): ?>
    <section class="featured">
	    <div class="row">    
	      <div class="featured-1 large-4 columns">
	        <?php print render($page['featured_1']); ?>
	      </div>
	      <div class="featured-2 large-4 columns">
	        <?php print render($page['featured_2']); ?>
	      </div>
	      <div class="featured-3 large-4 columns">
	        <?php print render($page['featured_3']); ?>
	      </div>
	        
	   </div>      
    </section>
  <?php endif; ?>

  <div class="full-width featured-bg">  
	 <a href="<?php print $front_page; ?>" id="main-logo"></a>        
     <?php print render($page['featured_bg']); ?>
  </div>
  
  <?php if (!empty($page['vm_slider'])): ?>
  <div class="full-width vm-slider">  
     <?php print render($page['vm_slider']); ?>
  </div>
  <?php endif; ?>
  

  <div class="full-width highlighted">    
    <section class="l-highlighted row">
      <div class="large-12 columns">
  
        <?php if ($breadcrumb): print $breadcrumb; endif; ?>


        <?php print render($title_prefix); ?>
        <h1 id="page-title" class="title"><?php print $title; ?></h1>
        <?php print render($title_suffix); ?>



	  <?php if (!empty($page['highlighted'])): ?>
		  <div class="highlight">        
			  <?php print render($page['highlighted']); ?>
		  </div>        
	  <?php endif; ?>
		<?php print render($page['campaign_partners']); ?>  	        
	  </div>
	</section> 
	<a href="#" class="next-section">Next Section</a>
  </div>	  
  
  
  <?php if ($messages && !$zurb_foundation_messages_modal): ?>
    <!--/.l-messages -->
    <section class="l-messages row">
      <div class="large-12 columns">
        <?php if ($messages): print $messages; endif; ?>
      </div>
    </section>
    <!--/.l-messages -->
  <?php endif; ?>

  <?php if (!empty($page['help'])): ?>
    <!--/.l-help -->
    <section class="l-help row">
      <div class="large-12 columns">
        <?php print render($page['help']); ?>
      </div>
    </section>
    <!--/.l-help -->
  <?php endif; ?>

  <main role="main" class="row l-main">
    <div class="<?php print $main_grid; ?> main columns">

      <a id="main-content"></a>



      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
        <?php if (!empty($tabs2)): print render($tabs2); endif; ?>
      <?php endif; ?>

      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>



      <?php print render($page['content']); ?>
      

    </div>
    <!--/.main region -->
    <?php if (!empty($page['sidebar_first'])): ?>
      <aside role="complementary" class="<?php print $sidebar_first_grid; ?> l-sidebar-first columns sidebar sidebar-subpage">
        <?php print render($page['sidebar_first']); ?>
      </aside>
    <?php endif; ?>

  </main>
  <!--/.main-->

  <?php if (!empty($page['section_1'])): ?>
    <!--/.section 1 -->
    <div class="full-width section-1">
		<a href="#" class="next-section">Next Section</a>        
	    <section class="l-section-1 row">
	      <div class="large-12 columns">
	        <?php print render($page['section_1']); ?>
	      </div>
	    </section>
    </div>
    <!--/.section 2 -->
  <?php endif; ?>


  <?php if (!empty($page['triptych_first']) || !empty($page['triptych_middle']) || !empty($page['triptych_last'])): ?>
    <!--.triptych-->
    <div class="full-width triptych">    
	    <section class="l-triptych row">
	      <div class="triptych-first large-4 columns">
	        <?php print render($page['triptych_first']); ?>
	      </div>
	      <div class="triptych-middle large-4 columns">
	        <?php print render($page['triptych_middle']); ?>
	      </div>
	      <div class="triptych-last large-4 columns">
	        <?php print render($page['triptych_last']); ?>
	      </div>
	    </section>
    </div>
    <!--/.triptych -->
  <?php endif; ?>


  <?php if (!empty($page['section_2'])): ?>
    <!--/.section 2 -->
    <div class="full-width section-2">
	    <section class="l-section-2 row">
	      <div class="large-12 columns">
	        <?php print render($page['section_2']); ?>
	      </div>
	    </section>
    </div>
    <!--/.section 2 -->
  <?php endif; ?>

  <!--.l-footer-->
  <div class="full-width footer-full-width">  
	  <footer class="l-footer row" role="contentinfo">
	    <?php if (!empty($page['footer'])): ?>
	      <div class="footer large-12 columns">
	        <?php print render($page['footer']); ?>
	      </div>
	    <?php endif; ?>
	  </footer>
  </div>
  <div class="full-width footer-partners">  
	  <a href="#" class="scrollToTop">Top</a>	    
	  <footer class="l-footer-partners row" role="contentinfo">
	    <?php if (!empty($page['partners'])): ?>
	      <div class="partners large-12 columns">
	        <?php print render($page['partners']); ?>
	      </div>
	    <?php endif; ?>
	  </footer>
  </div>  
  <div class="full-width below-footer-full-width">    
    <?php if (!empty($page['below_footer'])): ?>
      <div class="below-footer large-12 columns">
        <?php print render($page['below_footer']); ?>
      </div>
    <?php endif; ?>
  </div>  
  <!--/.footer-->

  <?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
</div>
<!--/.page -->
<script>
jQuery(".highlight .block").each(function(i) 
{     
	jQuery(this).addClass("block-" + i);
}); 

jQuery(".footer-partners a[href^='http://']").attr("target","_blank");
	
jQuery("aside .block").each(function(i) 
{     
	jQuery(this).addClass("block-" + i);
}); 

jQuery(".featured-bg .block").each(function(i) 
{     
	jQuery(this).addClass("block-" + i);
}); 
</script>