<?php
/**
 * @file
 * wise_book_nm.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wise_book_nm_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'nuclear_monitor_search';
  $view->description = 'Nuclear Monitor search and archive';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_nuclear_monitor_index';
  $view->human_name = 'Nuclear Monitor search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Nuclear Monitor';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['search_api_bypass_access'] = 0;
  $handler->display->display_options['query']['options']['entity_access'] = 0;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'load_more';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['effects']['speed'] = 'slow';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'views-row [type]';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_issue' => 'field_issue',
    'field_publication_date' => 'field_publication_date',
    'field_special_issue' => 'field_special_issue',
  );
  $handler->display->display_options['row_options']['separator'] = '-';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 1;
  /* Field: Indexed Node: Number */
  $handler->display->display_options['fields']['field_number']['id'] = 'field_number';
  $handler->display->display_options['fields']['field_number']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_number']['field'] = 'field_number';
  $handler->display->display_options['fields']['field_number']['label'] = '';
  $handler->display->display_options['fields']['field_number']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_number']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_number']['alter']['text'] = ': [field_number]';
  $handler->display->display_options['fields']['field_number']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_number']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Indexed Node: Nuclear Monitor Issue */
  $handler->display->display_options['fields']['field_issue']['id'] = 'field_issue';
  $handler->display->display_options['fields']['field_issue']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_issue']['field'] = 'field_issue';
  $handler->display->display_options['fields']['field_issue']['label'] = '';
  $handler->display->display_options['fields']['field_issue']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_issue']['alter']['text'] = '[field_issue][field_number]';
  $handler->display->display_options['fields']['field_issue']['element_label_colon'] = FALSE;
  /* Field: Indexed Node: Publication date */
  $handler->display->display_options['fields']['field_publication_date']['id'] = 'field_publication_date';
  $handler->display->display_options['fields']['field_publication_date']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_publication_date']['field'] = 'field_publication_date';
  $handler->display->display_options['fields']['field_publication_date']['label'] = '';
  $handler->display->display_options['fields']['field_publication_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_publication_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Indexed Node: Special issue */
  $handler->display->display_options['fields']['field_special_issue']['id'] = 'field_special_issue';
  $handler->display->display_options['fields']['field_special_issue']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_special_issue']['field'] = 'field_special_issue';
  $handler->display->display_options['fields']['field_special_issue']['label'] = '';
  $handler->display->display_options['fields']['field_special_issue']['element_label_colon'] = FALSE;
  /* Sort criterion: Indexed Node: Nuclear Monitor Issue */
  $handler->display->display_options['sorts']['field_issue']['id'] = 'field_issue';
  $handler->display->display_options['sorts']['field_issue']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['sorts']['field_issue']['field'] = 'field_issue';
  $handler->display->display_options['sorts']['field_issue']['order'] = 'DESC';
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['fields'] = array(
    'body:summary' => 'body:summary',
    'field_special_issue' => 'field_special_issue',
    'title' => 'title',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'Currently, no articles are tagged with the requested term or location.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 1;
  /* Field: Indexed Node: Number */
  $handler->display->display_options['fields']['field_number']['id'] = 'field_number';
  $handler->display->display_options['fields']['field_number']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_number']['field'] = 'field_number';
  $handler->display->display_options['fields']['field_number']['label'] = '';
  $handler->display->display_options['fields']['field_number']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_number']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_number']['alter']['text'] = ': [field_number]';
  $handler->display->display_options['fields']['field_number']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_number']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Indexed Node: Nuclear Monitor Issue */
  $handler->display->display_options['fields']['field_issue']['id'] = 'field_issue';
  $handler->display->display_options['fields']['field_issue']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_issue']['field'] = 'field_issue';
  $handler->display->display_options['fields']['field_issue']['label'] = '';
  $handler->display->display_options['fields']['field_issue']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_issue']['alter']['text'] = '[field_issue][field_number]';
  $handler->display->display_options['fields']['field_issue']['element_label_colon'] = FALSE;
  /* Field: Indexed Node: Publication date */
  $handler->display->display_options['fields']['field_publication_date']['id'] = 'field_publication_date';
  $handler->display->display_options['fields']['field_publication_date']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_publication_date']['field'] = 'field_publication_date';
  $handler->display->display_options['fields']['field_publication_date']['label'] = '';
  $handler->display->display_options['fields']['field_publication_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_publication_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Indexed Node: Special issue */
  $handler->display->display_options['fields']['field_special_issue']['id'] = 'field_special_issue';
  $handler->display->display_options['fields']['field_special_issue']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_special_issue']['field'] = 'field_special_issue';
  $handler->display->display_options['fields']['field_special_issue']['label'] = '';
  $handler->display->display_options['fields']['field_special_issue']['element_label_colon'] = FALSE;
  /* Field: Indexed Node: Content */
  $handler->display->display_options['fields']['field_content']['id'] = 'field_content';
  $handler->display->display_options['fields']['field_content']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_content']['field'] = 'field_content';
  $handler->display->display_options['fields']['field_content']['label'] = '';
  $handler->display->display_options['fields']['field_content']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_content']['alter']['text'] = '([field_content])';
  $handler->display->display_options['fields']['field_content']['element_label_colon'] = FALSE;
  $handler->display->display_options['path'] = 'nuclear-monitor';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Latest print issue';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 1;
  /* Field: Indexed Node: Number */
  $handler->display->display_options['fields']['field_number']['id'] = 'field_number';
  $handler->display->display_options['fields']['field_number']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_number']['field'] = 'field_number';
  $handler->display->display_options['fields']['field_number']['label'] = '';
  $handler->display->display_options['fields']['field_number']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_number']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_number']['alter']['text'] = ': [field_number]';
  $handler->display->display_options['fields']['field_number']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_number']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Indexed Node: Nuclear Monitor Issue */
  $handler->display->display_options['fields']['field_issue']['id'] = 'field_issue';
  $handler->display->display_options['fields']['field_issue']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_issue']['field'] = 'field_issue';
  $handler->display->display_options['fields']['field_issue']['label'] = '';
  $handler->display->display_options['fields']['field_issue']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_issue']['alter']['text'] = '[field_issue][field_number]';
  $handler->display->display_options['fields']['field_issue']['element_label_colon'] = FALSE;
  /* Field: Indexed Node: Publication date */
  $handler->display->display_options['fields']['field_publication_date']['id'] = 'field_publication_date';
  $handler->display->display_options['fields']['field_publication_date']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_publication_date']['field'] = 'field_publication_date';
  $handler->display->display_options['fields']['field_publication_date']['label'] = '';
  $handler->display->display_options['fields']['field_publication_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_publication_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Indexed Node: Special issue */
  $handler->display->display_options['fields']['field_special_issue']['id'] = 'field_special_issue';
  $handler->display->display_options['fields']['field_special_issue']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['field_special_issue']['field'] = 'field_special_issue';
  $handler->display->display_options['fields']['field_special_issue']['label'] = '';
  $handler->display->display_options['fields']['field_special_issue']['element_label_colon'] = FALSE;
  /* Field: The main body text: Text (indexed) */
  $handler->display->display_options['fields']['body_value']['id'] = 'body_value';
  $handler->display->display_options['fields']['body_value']['table'] = 'search_api_index_nuclear_monitor_index';
  $handler->display->display_options['fields']['body_value']['field'] = 'body_value';
  $handler->display->display_options['fields']['body_value']['label'] = '';
  $handler->display->display_options['fields']['body_value']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body_value']['link_to_entity'] = 0;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  $handler->display->display_options['block_description'] = 'Latest printed Nuclear Monitor';
  $export['nuclear_monitor_search'] = $view;

  return $export;
}
