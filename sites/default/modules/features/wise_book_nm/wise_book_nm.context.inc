<?php
/**
 * @file
 * wise_book_nm.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_book_nm_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'green_ball_nuclearmonitor';
  $context->description = '';
  $context->tag = 'Green ball';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'nuclear-monitor' => 'nuclear-monitor',
        'nuclear-monitor/*' => 'nuclear-monitor/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4310' => array(
          'module' => 'nodeblock',
          'delta' => '4310',
          'region' => 'sidebar_first',
          'weight' => '-58',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Green ball');
  $export['green_ball_nuclearmonitor'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nuclear_monitor';
  $context->description = '';
  $context->tag = 'Nuclear Monitor';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'nuclear-monitor' => 'nuclear-monitor',
        'nuclear-monitor/*' => 'nuclear-monitor/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-a7a99ff881d3d2e66204635db8267959' => array(
          'module' => 'views',
          'delta' => 'a7a99ff881d3d2e66204635db8267959',
          'region' => 'sidebar_first',
          'weight' => '-40',
        ),
        'facetapi-lxoDf3Wo1fXYG0WLbSyR3BvCSNp2xjr9' => array(
          'module' => 'facetapi',
          'delta' => 'lxoDf3Wo1fXYG0WLbSyR3BvCSNp2xjr9',
          'region' => 'sidebar_first',
          'weight' => '-49',
        ),
        'facetapi-zTu2ngkV5yD4ALjggQaA10Pw9kHY4SVi' => array(
          'module' => 'facetapi',
          'delta' => 'zTu2ngkV5yD4ALjggQaA10Pw9kHY4SVi',
          'region' => 'sidebar_first',
          'weight' => '-46',
        ),
        'facetapi-OEggjlw1IPVgvJd8TP44G1zgK0c9ASfO' => array(
          'module' => 'facetapi',
          'delta' => 'OEggjlw1IPVgvJd8TP44G1zgK0c9ASfO',
          'region' => 'sidebar_first',
          'weight' => '-45',
        ),
        'facetapi-TW3XObsmj2m1EUTUEzZ3gC1U4udXBEDp' => array(
          'module' => 'facetapi',
          'delta' => 'TW3XObsmj2m1EUTUEzZ3gC1U4udXBEDp',
          'region' => 'sidebar_first',
          'weight' => '-43',
        ),
        'facetapi-3OJgmm6V2bhCbJlN9PJKp6MfOQjdDTAl' => array(
          'module' => 'facetapi',
          'delta' => '3OJgmm6V2bhCbJlN9PJKp6MfOQjdDTAl',
          'region' => 'sidebar_first',
          'weight' => '-42',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Nuclear Monitor');
  $export['nuclear_monitor'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nuclear_monitor_landingpage';
  $context->description = '';
  $context->tag = 'Nuclear Monitor';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'nuclear-monitor' => 'nuclear-monitor',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-nuclear_monitor_search-block_1' => array(
          'module' => 'views',
          'delta' => 'nuclear_monitor_search-block_1',
          'region' => 'content',
          'weight' => '-42',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Nuclear Monitor');
  $export['nuclear_monitor_landingpage'] = $context;

  return $export;
}
