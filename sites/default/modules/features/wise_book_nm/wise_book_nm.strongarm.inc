<?php
/**
 * @file
 * wise_book_nm.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function wise_book_nm_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'facetapi_pretty_paths_searcher_search_api@nuclear_monitor_index';
  $strongarm->value = 1;
  $export['facetapi_pretty_paths_searcher_search_api@nuclear_monitor_index'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'facetapi_pretty_paths_searcher_search_api@nuclear_monitor_index_options';
  $strongarm->value = array(
    'sort_path_segments' => 1,
    'base_path_provider' => 'default',
  );
  $export['facetapi_pretty_paths_searcher_search_api@nuclear_monitor_index_options'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__book';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'teaser' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => TRUE,
      ),
      'print' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '14',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__book'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_book';
  $strongarm->value = '0';
  $export['language_content_type_book'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_book';
  $strongarm->value = array();
  $export['menu_options_book'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_book';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_book'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_book';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_book'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_book';
  $strongarm->value = '1';
  $export['node_preview_book'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_book';
  $strongarm->value = 0;
  $export['node_submitted_book'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_book_pattern';
  $strongarm->value = 'nuclear-monitor/[node:field_issue]/[node:title]';
  $export['pathauto_node_book_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'search_api_facets_search_ids';
  $strongarm->value = array(
    'nuclear_monitor_index' => array(
      'search_api_views:nuclear_monitor_search:default' => 'search_api_views:nuclear_monitor_search:default',
      'search_api_views:nuclear_monitor_search:panel_pane_1' => 'search_api_views:nuclear_monitor_search:panel_pane_1',
      'search_api_views:nuclear_monitor_search:page_1' => 'search_api_views:nuclear_monitor_search:page_1',
      'search_api_views:nuclear_monitor_search:block_1' => 'search_api_views:nuclear_monitor_search:block_1',
    ),
    'location_index' => array(
      'search_api_views:location_search:openlayers_1' => 'search_api_views:location_search:openlayers_1',
      'search_api_views:location_search:page_1' => 'search_api_views:location_search:page_1',
      'search_api_views:location_search:page' => 'search_api_views:location_search:page',
      'search_api_views:location_search:openlayers_2' => 'search_api_views:location_search:openlayers_2',
      'search_api_views:icon_test_view:page' => 'search_api_views:icon_test_view:page',
      'search_api_views:location_search_layers:openlayers_1' => 'search_api_views:location_search_layers:openlayers_1',
      'search_api_views:location_search_layers:openlayers_3' => 'search_api_views:location_search_layers:openlayers_3',
      'search_api_views:location_search_layers:openlayers_4' => 'search_api_views:location_search_layers:openlayers_4',
      'search_api_views:location_search_layers:openlayers_5' => 'search_api_views:location_search_layers:openlayers_5',
      'search_api_views:location_search_layers:openlayers_6' => 'search_api_views:location_search_layers:openlayers_6',
      'search_api_views:location_search_layers:openlayers_2' => 'search_api_views:location_search_layers:openlayers_2',
      'search_api_views:location_search:openlayers_3' => 'search_api_views:location_search:openlayers_3',
      'search_api_views:location_search:openlayers_4' => 'search_api_views:location_search:openlayers_4',
      'search_api_views:location_search:openlayers_5' => 'search_api_views:location_search:openlayers_5',
    ),
  );
  $export['search_api_facets_search_ids'] = $strongarm;

  return $export;
}
