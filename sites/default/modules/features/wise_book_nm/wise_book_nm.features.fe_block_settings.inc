<?php
/**
 * @file
 * wise_book_nm.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function wise_book_nm_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['facetapi-3OJgmm6V2bhCbJlN9PJKp6MfOQjdDTAl'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '3OJgmm6V2bhCbJlN9PJKp6MfOQjdDTAl',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'Tags',
    'visibility' => 0,
  );

  $export['facetapi-CBXdpR3706as0awi67lg0QEMaut0790P'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'CBXdpR3706as0awi67lg0QEMaut0790P',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'Issue',
    'visibility' => 0,
  );

  $export['facetapi-OEggjlw1IPVgvJd8TP44G1zgK0c9ASfO'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'OEggjlw1IPVgvJd8TP44G1zgK0c9ASfO',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'Publication date',
    'visibility' => 0,
  );

  $export['facetapi-TW3XObsmj2m1EUTUEzZ3gC1U4udXBEDp'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'TW3XObsmj2m1EUTUEzZ3gC1U4udXBEDp',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'Country',
    'visibility' => 0,
  );

  $export['facetapi-lxoDf3Wo1fXYG0WLbSyR3BvCSNp2xjr9'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'lxoDf3Wo1fXYG0WLbSyR3BvCSNp2xjr9',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'Type',
    'visibility' => 0,
  );

  $export['facetapi-zTu2ngkV5yD4ALjggQaA10Pw9kHY4SVi'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'zTu2ngkV5yD4ALjggQaA10Pw9kHY4SVi',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'About',
    'visibility' => 0,
  );

  $export['nodeblock-4310'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 4310,
    'module' => 'nodeblock',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-a7a99ff881d3d2e66204635db8267959'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'a7a99ff881d3d2e66204635db8267959',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-nuclear_monitor_search-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'nuclear_monitor_search-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
