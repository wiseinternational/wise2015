<?php
/**
 * @file
 * wise_book_nm.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_book_nm_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wise_book_nm_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wise_book_nm_node_info() {
  $items = array(
    'book' => array(
      'name' => t('Nuclear Monitor Article'),
      'base' => 'node_content',
      'description' => t('<em>Books</em> have a built-in hierarchical navigation. Use for handbooks or tutorials.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function wise_book_nm_default_search_api_index() {
  $items = array();
  $items['nuclear_monitor_index'] = entity_import('search_api_index', '{
    "name" : "Nuclear Monitor index",
    "machine_name" : "nuclear_monitor_index",
    "description" : null,
    "server" : "database",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "body:summary" : { "type" : "text" },
        "body:value" : { "type" : "text", "boost" : "0.8" },
        "field_about:title" : { "type" : "list\\u003Cstring\\u003E" },
        "field_author" : { "type" : "string" },
        "field_content" : { "type" : "string" },
        "field_country" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_issue" : { "type" : "string" },
        "field_number" : { "type" : "integer" },
        "field_publication_date" : { "type" : "date" },
        "field_special_issue" : { "type" : "text" },
        "field_tags" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "search_api_language" : { "type" : "string" },
        "title" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "book" : "book" } }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true, "field_special_issue" : true, "field_author" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "field_special_issue" : true, "field_author" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "field_special_issue" : true, "field_author" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 1,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "field_special_issue" : true, "field_author" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function wise_book_nm_default_search_api_server() {
  $items = array();
  $items['database'] = entity_import('search_api_server', '{
    "name" : "database",
    "machine_name" : "database",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "2",
      "partial_matches" : 0,
      "indexes" : {
        "nuclear_monitor_index" : {
          "title" : {
            "table" : "search_api_db_nuclear_monitor_index_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_tags" : {
            "table" : "search_api_db_nuclear_monitor_index_field_tags",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_country" : {
            "table" : "search_api_db_nuclear_monitor_index_field_country",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_publication_date" : {
            "table" : "search_api_db_nuclear_monitor_index",
            "column" : "field_publication_date",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_issue" : {
            "table" : "search_api_db_nuclear_monitor_index",
            "column" : "field_issue_1",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_special_issue" : {
            "table" : "search_api_db_nuclear_monitor_index_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_author" : {
            "table" : "search_api_db_nuclear_monitor_index",
            "type" : "string",
            "boost" : "1.0",
            "column" : "field_author"
          },
          "field_content" : {
            "table" : "search_api_db_nuclear_monitor_index",
            "column" : "field_content",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_number" : {
            "table" : "search_api_db_nuclear_monitor_index",
            "column" : "field_number",
            "type" : "integer",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_nuclear_monitor_index",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "body:summary" : {
            "table" : "search_api_db_nuclear_monitor_index_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "body:value" : {
            "table" : "search_api_db_nuclear_monitor_index_text",
            "type" : "text",
            "boost" : "0.8"
          },
          "field_about:title" : {
            "table" : "search_api_db_nuclear_monitor_index_field_about_title",
            "column" : "value",
            "type" : "list\\u003Cstring\\u003E",
            "boost" : "1.0"
          }
        },
        "location_index" : {
          "nid" : {
            "table" : "search_api_db_location_index",
            "column" : "nid",
            "type" : "integer",
            "boost" : "1.0"
          },
          "title" : {
            "table" : "search_api_db_location_index_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_location_index",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_tags" : {
            "table" : "search_api_db_location_index_field_tags",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_alternative_name" : {
            "table" : "search_api_db_location_index_text",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "1.0"
          },
          "field_wise_office" : {
            "table" : "search_api_db_location_index",
            "column" : "field_wise_office",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_type" : {
            "table" : "search_api_db_location_index_field_type_1",
            "column" : "field_type",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_capacity" : {
            "table" : "search_api_db_location_index",
            "type" : "integer",
            "boost" : "0.5",
            "column" : "field_capacity"
          },
          "field_email" : {
            "table" : "search_api_db_location_index_text",
            "type" : "text",
            "boost" : "0.3"
          },
          "field_opening_year" : {
            "table" : "search_api_db_location_index",
            "column" : "field_opening_year_1",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_owner" : {
            "table" : "search_api_db_location_index",
            "type" : "string",
            "boost" : "1.0",
            "column" : "field_owner"
          },
          "field_utility" : {
            "table" : "search_api_db_location_index",
            "type" : "string",
            "boost" : "0.8",
            "column" : "field_utility"
          },
          "field_comment" : {
            "table" : "search_api_db_location_index_text",
            "type" : "text",
            "boost" : "0.8"
          },
          "field_work_area" : {
            "table" : "search_api_db_location_index_field_work_area",
            "column" : "value",
            "type" : "list\\u003Cstring\\u003E",
            "boost" : "1.0"
          },
          "field_campaign" : {
            "table" : "search_api_db_location_index_text",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "1.0"
          },
          "field_nuclear_installation_type" : {
            "table" : "search_api_db_location_index",
            "column" : "field_nuclear_installation_type",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_fuel" : {
            "table" : "search_api_db_location_index",
            "column" : "field_fuel",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_status" : {
            "table" : "search_api_db_location_index",
            "column" : "field_status",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_enrichment_technique" : {
            "table" : "search_api_db_location_index",
            "column" : "field_enrichment_technique",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_swu" : {
            "table" : "search_api_db_location_index",
            "column" : "field_swu",
            "type" : "decimal",
            "boost" : "1.0"
          },
          "field_reprocessing_technique" : {
            "table" : "search_api_db_location_index",
            "column" : "field_reprocessing_technique",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_closing_year" : {
            "table" : "search_api_db_location_index",
            "column" : "field_closing_year",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_uranium_capacity" : {
            "table" : "search_api_db_location_index",
            "column" : "field_uranium_capacity",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_plutonium_capacity" : {
            "table" : "search_api_db_location_index",
            "column" : "field_plutonium_capacity",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_operator" : {
            "table" : "search_api_db_location_index",
            "column" : "field_operator",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_share_of_world_production" : {
            "table" : "search_api_db_location_index",
            "column" : "field_share_of_world_production",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_mining_technique" : {
            "table" : "search_api_db_location_index",
            "column" : "field_mining_technique",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_storage_method" : {
            "table" : "search_api_db_location_index",
            "column" : "field_storage_method",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_culprit" : {
            "table" : "search_api_db_location_index_text",
            "type" : "text",
            "boost" : "8.0"
          },
          "field_dumped" : {
            "table" : "search_api_db_location_index_text",
            "type" : "text",
            "boost" : "8.0"
          },
          "field_production" : {
            "table" : "search_api_db_location_index",
            "column" : "field_production",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_waste" : {
            "table" : "search_api_db_location_index_field_waste",
            "column" : "value",
            "type" : "list\\u003Cstring\\u003E",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
