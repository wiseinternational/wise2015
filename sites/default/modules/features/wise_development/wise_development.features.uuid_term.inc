<?php
/**
 * @file
 * wise_development.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function wise_development_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Nuclear waste facility',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '3991cd19-ab44-42df-8ef2-b33984b21571',
    'vocabulary_machine_name' => 'place_type',
  );
  $terms[] = array(
    'name' => 'Newspaper',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '475d0fb7-75c9-486f-8502-1b1402505723',
    'vocabulary_machine_name' => 'group_type',
  );
  $terms[] = array(
    'name' => 'Medical nuclear reactor',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '6a6cc3d8-197b-4e8b-b9cd-a9f5a1d29415',
    'vocabulary_machine_name' => 'place_type',
  );
  $terms[] = array(
    'name' => 'Uranium enrichment facility',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '80a04054-c23f-4e7d-9b50-bbcb2425c525',
    'vocabulary_machine_name' => 'place_type',
  );
  $terms[] = array(
    'name' => 'UK',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '81dfcbfb-8661-49de-a425-fb86274e6e12',
    'vocabulary_machine_name' => 'country',
  );
  $terms[] = array(
    'name' => 'Germany',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '83bf5c91-677a-43dc-aa03-b763d01b5009',
    'vocabulary_machine_name' => 'country',
  );
  $terms[] = array(
    'name' => 'Uranium mine',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'a8774740-977a-4f56-996f-8e89e06787f2',
    'vocabulary_machine_name' => 'place_type',
  );
  $terms[] = array(
    'name' => 'Thorium mine',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'b2299525-9e52-4741-b164-67c0ab29ce4a',
    'vocabulary_machine_name' => 'place_type',
  );
  $terms[] = array(
    'name' => 'Nuclear power station (closed)',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'b6d78c7a-8379-4950-9534-193f3632aa93',
    'vocabulary_machine_name' => 'place_type',
  );
  $terms[] = array(
    'name' => 'Nuclear power station',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'd65bf08f-6caa-4cc0-b921-3ecd9328d7c2',
    'vocabulary_machine_name' => 'place_type',
  );
  $terms[] = array(
    'name' => 'NGO',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd8f516e5-dcdb-47f9-842c-a0bea96c8408',
    'vocabulary_machine_name' => 'group_type',
  );
  $terms[] = array(
    'name' => 'accident',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e524b40c-1258-43f3-9193-55af2bcce223',
    'vocabulary_machine_name' => 'incident_type',
  );
  $terms[] = array(
    'name' => 'Netherlands',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'f83596f6-a961-4d3d-9a45-8d3b567e258a',
    'vocabulary_machine_name' => 'country',
  );
  return $terms;
}
