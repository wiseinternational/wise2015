<?php
/**
 * @file
 * wise_development.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function wise_development_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 75,
  'title' => 'Wise Germany',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '4587c82a-db93-4b48-adf2-6dd7b45ba562',
  'type' => 'group',
  'language' => 'und',
  'created' => 1427218132,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '30bf4bcc-f856-4ee0-8022-d1ee56cfb427',
  'revision_uid' => 75,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>WISE Uranium Project is part of World Information Service on Energy.<br />
It covers the health and environmental impacts of uranium mining and nuclear fuel production.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>WISE Uranium Project is part of World Information Service on Energy.<br />
It covers the health and environmental impacts of uranium mining and nuclear fuel production.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_alternative_name' => array(
    'und' => array(
      0 => array(
        'value' => 'Wise Uranium Project',
        'format' => NULL,
        'safe_value' => 'Wise Uranium Project',
      ),
    ),
  ),
  'field_country' => array(
    'und' => array(
      0 => array(
        'tid' => 22,
        'uuid' => '83bf5c91-677a-43dc-aa03-b763d01b5009',
      ),
    ),
  ),
  'field_links' => array(
    'und' => array(
      0 => array(
        'url' => 'http://www.wise-uranium.org/',
        'title' => NULL,
        'attributes' => array(),
      ),
    ),
  ),
  'field_logo' => array(),
  'field_location' => array(
    'und' => array(
      0 => array(
        'geom' => 'POINT (11.513671860168 48.467094560095)',
        'geo_type' => 'point',
        'lat' => 48.467094560095,
        'lon' => 11.513671860168,
        'left' => 11.513671860168,
        'top' => 48.467094560095,
        'right' => 11.513671860168,
        'bottom' => 48.467094560095,
        'geohash' => 'u285tzpvrcrbxsu9',
      ),
    ),
  ),
  'field_wise_office' => array(
    'und' => array(
      0 => array(
        'value' => 'yes',
      ),
    ),
  ),
  'field_group_type' => array(
    'und' => array(
      0 => array(
        'tid' => 44,
        'uuid' => 'd8f516e5-dcdb-47f9-842c-a0bea96c8408',
      ),
    ),
  ),
  'field_tags' => array(
    'und' => array(
      0 => array(
        'tid' => 31,
        'uuid' => 'c5397073-6a85-4b8f-88bc-5f7e12d157d6',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'name' => 'ifrik',
  'picture' => 0,
  'data' => 'a:5:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";}',
  'date' => '2015-03-24 18:28:52 +0100',
);
  $nodes[] = array(
  'uid' => 75,
  'title' => 'Nekarwestheim antiatom',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '2ddb678c-f654-455b-831a-b274d4ce29f4',
  'type' => 'group',
  'language' => 'und',
  'created' => 1427217771,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '38c5c9f5-ea8a-4faa-8fbf-e26f396bd7fb',
  'revision_uid' => 75,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>des Aktionsb&uuml;ndnis CASTOR-Widerstand Neckarwestheim (oder der S&uuml;dwestdeutschen Antiatom-Initiativen)</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>des Aktionsbündnis CASTOR-Widerstand Neckarwestheim (oder der Südwestdeutschen Antiatom-Initiativen)</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_alternative_name' => array(),
  'field_country' => array(
    'und' => array(
      0 => array(
        'tid' => 22,
        'uuid' => '83bf5c91-677a-43dc-aa03-b763d01b5009',
      ),
    ),
  ),
  'field_links' => array(
    'und' => array(
      0 => array(
        'url' => 'http://neckarwestheim.antiatom.net/',
        'title' => NULL,
        'attributes' => array(),
      ),
    ),
  ),
  'field_logo' => array(),
  'field_location' => array(
    'und' => array(
      0 => array(
        'geom' => 'POINT (9.1892051321071 49.048662041557)',
        'geo_type' => 'point',
        'lat' => 49.048662041557,
        'lon' => 9.189205132107,
        'left' => 9.189205132107,
        'top' => 49.048662041557,
        'right' => 9.189205132107,
        'bottom' => 49.048662041557,
        'geohash' => 'u0wx11535vrcz1g6',
      ),
    ),
  ),
  'field_wise_office' => array(
    'und' => array(
      0 => array(
        'value' => 'no',
      ),
    ),
  ),
  'field_group_type' => array(
    'und' => array(
      0 => array(
        'tid' => 45,
        'uuid' => '475d0fb7-75c9-486f-8502-1b1402505723',
      ),
    ),
  ),
  'field_tags' => array(
    'und' => array(
      0 => array(
        'tid' => 46,
        'uuid' => 'baa307f5-3f7e-41a2-9c73-1b9e6da476ee',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'name' => 'ifrik',
  'picture' => 0,
  'data' => 'a:5:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";}',
  'date' => '2015-03-24 18:22:51 +0100',
);
  $nodes[] = array(
  'uid' => 75,
  'title' => 'Calder Hall',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '59431b1d-0811-4f8c-9f32-e8864820377f',
  'type' => 'place',
  'language' => 'und',
  'created' => 1427221096,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '5e18ff0e-1dd7-4e51-92b0-1b8b6cdffd3c',
  'revision_uid' => 75,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Calder Hall, first connected to the grid on 27 August 1956 and officially opened by Queen Elizabeth II on 17 October 1956, was the world&#39;s first power station to generate electricity on an industrial scale (four 60 MWe reactors) from nuclear energy;</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Calder Hall, first connected to the grid on 27 August 1956 and officially opened by Queen Elizabeth II on 17 October 1956, was the world\'s first power station to generate electricity on an industrial scale (four 60 MWe reactors) from nuclear energy;</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_alternative_name' => array(
    'und' => array(
      0 => array(
        'value' => 'Calder Hall nuclear power station',
        'format' => NULL,
        'safe_value' => 'Calder Hall nuclear power station',
      ),
    ),
  ),
  'field_country' => array(
    'und' => array(
      0 => array(
        'tid' => 26,
        'uuid' => '81dfcbfb-8661-49de-a425-fb86274e6e12',
      ),
    ),
  ),
  'field_group' => array(),
  'field_links' => array(
    'und' => array(
      0 => array(
        'url' => 'http://en.wikipedia.org/wiki/Sellafield#Calder_Hall_nuclear_power_station',
        'title' => NULL,
        'attributes' => array(),
      ),
    ),
  ),
  'field_location' => array(
    'und' => array(
      0 => array(
        'geom' => 'POLYGON ((-3.5079002733416 54.415776476076, -3.5060119981955 54.426363405719, -3.5010338182647 54.430058199604, -3.4905624742725 54.429658778488, -3.4888458605032 54.422668278677, -3.4888458605032 54.416975133677, -3.4895325060109 54.410781692144, -3.4998321886262 54.407684620516, -3.5079002733416 54.415776476076))',
        'geo_type' => 'polygon',
        'lat' => 54.419446461931,
        'lon' => -3.497685479342,
        'left' => -3.507900273342,
        'top' => 54.430058199604,
        'right' => -3.488845860503,
        'bottom' => 54.407684620516,
        'geohash' => 'gctt8',
      ),
    ),
  ),
  'field_place_type' => array(
    'und' => array(
      0 => array(
        'tid' => 36,
        'uuid' => 'b6d78c7a-8379-4950-9534-193f3632aa93',
      ),
    ),
  ),
  'field_tags' => array(
    'und' => array(
      0 => array(
        'tid' => 42,
        'uuid' => '3c8d14d6-608b-4abf-82a7-fb80b0b5f18a',
      ),
      1 => array(
        'tid' => 7,
        'uuid' => 'dd8f8dac-1287-4c06-abdc-07669aa0f853',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'name' => 'ifrik',
  'picture' => 0,
  'data' => 'a:5:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";}',
  'date' => '2015-03-24 19:18:16 +0100',
);
  $nodes[] = array(
  'uid' => 75,
  'title' => 'Windscale fire',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'c1e039e1-a1b2-4205-b8bf-6407766c5e0b',
  'type' => 'incident',
  'language' => 'und',
  'created' => 1427218539,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '5e38f12b-d66c-469e-af6b-3cbafcd5541c',
  'revision_uid' => 75,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Fire.....</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Fire.....</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_country' => array(
    'und' => array(
      0 => array(
        'tid' => 26,
        'uuid' => '81dfcbfb-8661-49de-a425-fb86274e6e12',
      ),
    ),
  ),
  'field_date' => array(
    'und' => array(
      0 => array(
        'value' => '1957-10-07 00:00:00',
        'value2' => '1957-10-10 00:00:00',
        'timezone' => 'Europe/Paris',
        'timezone_db' => 'Europe/Paris',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_image' => array(),
  'field_links' => array(),
  'field_location' => array(
    'und' => array(
      0 => array(
        'geom' => 'POINT (-3.5000897027394 54.424854173345)',
        'geo_type' => 'point',
        'lat' => 54.424854173345,
        'lon' => -3.500089702739,
        'left' => -3.500089702739,
        'top' => 54.424854173345,
        'right' => -3.500089702739,
        'bottom' => 54.424854173345,
        'geohash' => 'gctt87ercbpvn9un',
      ),
    ),
  ),
  'field_tags' => array(
    'und' => array(
      0 => array(
        'tid' => 29,
        'uuid' => 'af63d892-6261-4d7e-bd00-968a3800d618',
      ),
      1 => array(
        'tid' => 43,
        'uuid' => 'd2b6fbb8-a2db-4a09-8896-578c2cc3e16a',
      ),
    ),
  ),
  'field_incident_type' => array(
    'und' => array(
      0 => array(
        'tid' => 27,
        'uuid' => 'e524b40c-1258-43f3-9193-55af2bcce223',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'name' => 'ifrik',
  'picture' => 0,
  'data' => 'a:5:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";}',
  'date' => '2015-03-24 18:35:39 +0100',
);
  $nodes[] = array(
  'uid' => 75,
  'title' => 'Lingen II',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'd94851c0-d138-406e-9e17-11e4fa15eae1',
  'type' => 'place',
  'language' => 'und',
  'created' => 1427218783,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '98010412-1587-49aa-aea5-d09c0653eb59',
  'revision_uid' => 75,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p><br />
Das Kraftwerk wurde als Ersatz f&uuml;r das 1977 stillgelegte Kernkraftwerk Lingen geplant und Anfang der 1980er Jahre gebaut. Der Reaktor wurde am 14. April 1988 zum ersten Mal <a href="https://de.wikipedia.org/wiki/Kritikalit%C3%A4t" title="Kritikalität">kritisch</a> und nahm am 20.&nbsp;Juni 1988 den kommerziellen Betrieb auf. Das Kraftwerk besitzt einen Druckwasserreaktor vom Typ Konvoi, der 4. Druckwasserreaktor-Generation in Deutschland. Im Reaktor befinden sich 193 Brennelemente mit einem Schwermetallgewicht von insgesamt 103&nbsp;Tonnen. Das Kernkraftwerk Emsland hat eine elektrische Leistung von 1.400&nbsp;MW (brutto). Abz&uuml;glich des Eigenbedarfes von 71&nbsp;MW werden bis zu 1.329&nbsp;MW in das Verbundnetz eingespeist. Der Netzanschluss erfolgt &uuml;ber die <a href="https://de.wikipedia.org/wiki/Schaltanlage" title="Schaltanlage">Schaltanlage</a> <a href="https://de.wikipedia.org/wiki/Hanekenf%C3%A4hr" title="Hanekenfähr">Hanekenf&auml;hr</a> auf der 380-kV-H&ouml;chstspannungsebene in das Stromnetz des &Uuml;bertragungsnetzbetreibers Amprion. Au&szlig;erdem steht der Anlage ein 152 Meter hoher Naturzug-Nassk&uuml;hlturm zur Verf&uuml;gung, welcher als R&uuml;ckk&uuml;hlanlage und im Ablaufbetrieb einsetzbar ist.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>
Das Kraftwerk wurde als Ersatz für das 1977 stillgelegte Kernkraftwerk Lingen geplant und Anfang der 1980er Jahre gebaut. Der Reaktor wurde am 14. April 1988 zum ersten Mal <a href="https://de.wikipedia.org/wiki/Kritikalit%C3%A4t" title="Kritikalität">kritisch</a> und nahm am 20. Juni 1988 den kommerziellen Betrieb auf. Das Kraftwerk besitzt einen Druckwasserreaktor vom Typ Konvoi, der 4. Druckwasserreaktor-Generation in Deutschland. Im Reaktor befinden sich 193 Brennelemente mit einem Schwermetallgewicht von insgesamt 103 Tonnen. Das Kernkraftwerk Emsland hat eine elektrische Leistung von 1.400 MW (brutto). Abzüglich des Eigenbedarfes von 71 MW werden bis zu 1.329 MW in das Verbundnetz eingespeist. Der Netzanschluss erfolgt über die <a href="https://de.wikipedia.org/wiki/Schaltanlage" title="Schaltanlage">Schaltanlage</a> <a href="https://de.wikipedia.org/wiki/Hanekenf%C3%A4hr" title="Hanekenfähr">Hanekenfähr</a> auf der 380-kV-Höchstspannungsebene in das Stromnetz des Übertragungsnetzbetreibers Amprion. Außerdem steht der Anlage ein 152 Meter hoher Naturzug-Nasskühlturm zur Verfügung, welcher als Rückkühlanlage und im Ablaufbetrieb einsetzbar ist.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_alternative_name' => array(
    'und' => array(
      0 => array(
        'value' => 'Kernkraftwerk Emsland',
        'format' => NULL,
        'safe_value' => 'Kernkraftwerk Emsland',
      ),
      1 => array(
        'value' => 'Nuclear Power station Emsland',
        'format' => NULL,
        'safe_value' => 'Nuclear Power station Emsland',
      ),
    ),
  ),
  'field_country' => array(
    'und' => array(
      0 => array(
        'tid' => 22,
        'uuid' => '83bf5c91-677a-43dc-aa03-b763d01b5009',
      ),
    ),
  ),
  'field_group' => array(
    'und' => array(
      0 => array(
        'target_id' => 4266,
      ),
    ),
  ),
  'field_links' => array(
    'und' => array(
      0 => array(
        'url' => 'https://de.wikipedia.org/wiki/Kernkraftwerk_Emsland',
        'title' => NULL,
        'attributes' => array(),
      ),
    ),
  ),
  'field_location' => array(
    'und' => array(
      0 => array(
        'geom' => 'POINT (7.3171949024365 52.472893371274)',
        'geo_type' => 'point',
        'lat' => 52.472893371274,
        'lon' => 7.317194902437,
        'left' => 7.317194902437,
        'top' => 52.472893371274,
        'right' => 7.317194902437,
        'bottom' => 52.472893371274,
        'geohash' => 'u1m4w82hybrcr4kr',
      ),
    ),
  ),
  'field_place_type' => array(
    'und' => array(
      0 => array(
        'tid' => 35,
        'uuid' => 'd65bf08f-6caa-4cc0-b921-3ecd9328d7c2',
      ),
    ),
  ),
  'field_tags' => array(
    'und' => array(
      0 => array(
        'tid' => 29,
        'uuid' => 'af63d892-6261-4d7e-bd00-968a3800d618',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'name' => 'ifrik',
  'picture' => 0,
  'data' => 'a:5:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";}',
  'date' => '2015-03-24 18:39:43 +0100',
);
  $nodes[] = array(
  'uid' => 75,
  'title' => 'Kernenergiecentrale Borssele',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '7d1e2417-b59e-41ae-a3d6-b5bd182a40a1',
  'type' => 'place',
  'language' => 'und',
  'created' => 1427218675,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '9b1e5f1f-08c0-48a3-aa95-9bcf9aef31bb',
  'revision_uid' => 75,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>In 1969 bestelde de Provinciale Zeeuwse Electriciteitsproductiemaatschappij (PZEM) een kerncentrale.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>In 1969 bestelde de Provinciale Zeeuwse Electriciteitsproductiemaatschappij (PZEM) een kerncentrale.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_alternative_name' => array(),
  'field_country' => array(
    'und' => array(
      0 => array(
        'tid' => 28,
        'uuid' => 'f83596f6-a961-4d3d-9a45-8d3b567e258a',
      ),
    ),
  ),
  'field_group' => array(),
  'field_links' => array(
    'und' => array(
      0 => array(
        'url' => 'https://nl.wikipedia.org/wiki/Kerncentrale_Borssele',
        'title' => NULL,
        'attributes' => array(),
      ),
    ),
  ),
  'field_location' => array(
    'und' => array(
      0 => array(
        'geom' => 'POINT (3.7141942545238 51.432631614175)',
        'geo_type' => 'point',
        'lat' => 51.432631614175,
        'lon' => 3.714194254524,
        'left' => 3.714194254524,
        'top' => 51.432631614175,
        'right' => 3.714194254524,
        'bottom' => 51.432631614175,
        'geohash' => 'u14sse08fcrynzwc',
      ),
    ),
  ),
  'field_place_type' => array(
    'und' => array(
      0 => array(
        'tid' => 35,
        'uuid' => 'd65bf08f-6caa-4cc0-b921-3ecd9328d7c2',
      ),
    ),
  ),
  'field_tags' => array(
    'und' => array(
      0 => array(
        'tid' => 29,
        'uuid' => 'af63d892-6261-4d7e-bd00-968a3800d618',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'name' => 'ifrik',
  'picture' => 0,
  'data' => 'a:5:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";}',
  'date' => '2015-03-24 18:37:55 +0100',
);
  return $nodes;
}
