<?php
/**
 * @file
 * wise_nuclear_energy.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_nuclear_energy_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
