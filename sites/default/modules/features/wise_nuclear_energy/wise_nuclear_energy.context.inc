<?php
/**
 * @file
 * wise_nuclear_energy.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_nuclear_energy_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nuclear_energy_detailpages';
  $context->description = '';
  $context->tag = 'Website section';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~nuclear-energy/nuclear-energy' => '~nuclear-energy/nuclear-energy',
        'nuclear-energy/*' => 'nuclear-energy/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-6093' => array(
          'module' => 'nodeblock',
          'delta' => '6093',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
        'menu-menu-nuclear-energy-' => array(
          'module' => 'menu',
          'delta' => 'menu-nuclear-energy-',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Website section');
  $export['nuclear_energy_detailpages'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nuclear_energy_landingpage';
  $context->description = '';
  $context->tag = 'Website section';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'nuclear-energy/nuclear-energy' => 'nuclear-energy/nuclear-energy',
        'nuclear-energy' => 'nuclear-energy',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-6093' => array(
          'module' => 'nodeblock',
          'delta' => '6093',
          'region' => 'featured_bg',
          'weight' => '-9',
        ),
        'menu-menu-nuclear-energy-' => array(
          'module' => 'menu',
          'delta' => 'menu-nuclear-energy-',
          'region' => 'content',
          'weight' => '-10',
        ),
        'menu-menu-publications' => array(
          'module' => 'menu',
          'delta' => 'menu-publications',
          'region' => 'content',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Website section');
  $export['nuclear_energy_landingpage'] = $context;

  return $export;
}
