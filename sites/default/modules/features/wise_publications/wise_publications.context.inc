<?php
/**
 * @file
 * wise_publications.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_publications_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'publications';
  $context->description = '';
  $context->tag = 'Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'nuclear-energy/publications' => 'nuclear-energy/publications',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-publications-block' => array(
          'module' => 'views',
          'delta' => 'publications-block',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Page');
  $export['publications'] = $context;

  return $export;
}
