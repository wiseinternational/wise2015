<?php
/**
 * @file
 * wise_campaign.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function wise_campaign_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-campaign-menu.
  $menus['menu-campaign-menu'] = array(
    'menu_name' => 'menu-campaign-menu',
    'title' => 'Campaign menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Campaign menu');


  return $menus;
}
