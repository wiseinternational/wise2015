<?php
/**
 * @file
 * wise_campaign.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function wise_campaign_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-funding_partners_campaign-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'funding_partners_campaign-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-partners_campaign-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'partners_campaign-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
