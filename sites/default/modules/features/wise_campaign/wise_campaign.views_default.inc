<?php
/**
 * @file
 * wise_campaign.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wise_campaign_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'funding_partners_campaign';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Funding partners campaign';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'These foundations and organisations support the campaign financially ';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: link */
  $handler->display->display_options['fields']['field_link_partner']['id'] = 'field_link_partner';
  $handler->display->display_options['fields']['field_link_partner']['table'] = 'field_data_field_link_partner';
  $handler->display->display_options['fields']['field_link_partner']['field'] = 'field_link_partner';
  $handler->display->display_options['fields']['field_link_partner']['label'] = '';
  $handler->display->display_options['fields']['field_link_partner']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_link_partner']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_link_partner']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_link_partner']['type'] = 'link_absolute';
  /* Field: Content: Partner image */
  $handler->display->display_options['fields']['field_partner_image']['id'] = 'field_partner_image';
  $handler->display->display_options['fields']['field_partner_image']['table'] = 'field_data_field_partner_image';
  $handler->display->display_options['fields']['field_partner_image']['field'] = 'field_partner_image';
  $handler->display->display_options['fields']['field_partner_image']['label'] = '';
  $handler->display->display_options['fields']['field_partner_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_partner_image']['alter']['path'] = '[field_link_partner]';
  $handler->display->display_options['fields']['field_partner_image']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_partner_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_partner_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_partner_image']['settings'] = array(
    'image_style' => 'partners_campaign',
    'image_link' => '',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'funding_partner_logo_s' => 'funding_partner_logo_s',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['funding_partners_campaign'] = $view;

  $view = new view();
  $view->name = 'partners_campaign';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Partners campaign';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Launching partners';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Website */
  $handler->display->display_options['fields']['field_website_link']['id'] = 'field_website_link';
  $handler->display->display_options['fields']['field_website_link']['table'] = 'field_data_field_website_link';
  $handler->display->display_options['fields']['field_website_link']['field'] = 'field_website_link';
  $handler->display->display_options['fields']['field_website_link']['label'] = '';
  $handler->display->display_options['fields']['field_website_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_website_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_website_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_website_link']['type'] = 'link_absolute';
  /* Field: Content: Partner image */
  $handler->display->display_options['fields']['field_partner_image']['id'] = 'field_partner_image';
  $handler->display->display_options['fields']['field_partner_image']['table'] = 'field_data_field_partner_image';
  $handler->display->display_options['fields']['field_partner_image']['field'] = 'field_partner_image';
  $handler->display->display_options['fields']['field_partner_image']['label'] = '';
  $handler->display->display_options['fields']['field_partner_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_partner_image']['alter']['path'] = '[field_website_link]';
  $handler->display->display_options['fields']['field_partner_image']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['field_partner_image']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_partner_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_partner_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_partner_image']['settings'] = array(
    'image_style' => 'partners_campaign',
    'image_link' => '',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'partners_campaign' => 'partners_campaign',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['partners_campaign'] = $view;

  return $export;
}
