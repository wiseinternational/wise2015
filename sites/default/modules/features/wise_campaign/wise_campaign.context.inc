<?php
/**
 * @file
 * wise_campaign.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_campaign_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'campaign_detailpage';
  $context->description = '';
  $context->tag = 'Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'campaign/*' => 'campaign/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4458' => array(
          'module' => 'nodeblock',
          'delta' => '4458',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
        'menu-menu-campaign-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-campaign-menu',
          'region' => 'sidebar_first',
          'weight' => '3',
        ),
        'nodeblock-4342' => array(
          'module' => 'nodeblock',
          'delta' => '4342',
          'region' => 'sidebar_first',
          'weight' => '4',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Page');
  $export['campaign_detailpage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'campaign_landingpage';
  $context->description = '';
  $context->tag = 'Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'campaign' => 'campaign',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4458' => array(
          'module' => 'nodeblock',
          'delta' => '4458',
          'region' => 'featured_bg',
          'weight' => '-46',
        ),
        'views-bestsellers-block_1' => array(
          'module' => 'views',
          'delta' => 'bestsellers-block_1',
          'region' => 'vm_slider',
          'weight' => '-10',
        ),
        'nodeblock-4899' => array(
          'module' => 'nodeblock',
          'delta' => '4899',
          'region' => 'highlighted',
          'weight' => '-61',
        ),
        'nodeblock-4451' => array(
          'module' => 'nodeblock',
          'delta' => '4451',
          'region' => 'highlighted',
          'weight' => '-60',
        ),
        'nodeblock-4452' => array(
          'module' => 'nodeblock',
          'delta' => '4452',
          'region' => 'highlighted',
          'weight' => '-59',
        ),
        'nodeblock-4898' => array(
          'module' => 'nodeblock',
          'delta' => '4898',
          'region' => 'highlighted',
          'weight' => '-58',
        ),
        'nodeblock-4342' => array(
          'module' => 'nodeblock',
          'delta' => '4342',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'nodeblock-4326' => array(
          'module' => 'nodeblock',
          'delta' => '4326',
          'region' => 'section_1',
          'weight' => '-8',
        ),
        'nodeblock-4327' => array(
          'module' => 'nodeblock',
          'delta' => '4327',
          'region' => 'section_1',
          'weight' => '-7',
        ),
        'nodeblock-4328' => array(
          'module' => 'nodeblock',
          'delta' => '4328',
          'region' => 'section_1',
          'weight' => '-6',
        ),
        'nodeblock-4329' => array(
          'module' => 'nodeblock',
          'delta' => '4329',
          'region' => 'section_1',
          'weight' => '-5',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Page');
  $export['campaign_landingpage'] = $context;

  return $export;
}
