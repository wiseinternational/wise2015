<?php
/**
 * @file
 * wise_campaign.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_campaign_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wise_campaign_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wise_campaign_node_info() {
  $items = array(
    'funding_partner_logo_s' => array(
      'name' => t('Funding partner logo\'s'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'partner' => array(
      'name' => t('Partner'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'partners_campaign' => array(
      'name' => t('Partners campaign'),
      'base' => 'node_content',
      'description' => t('Campaign launching partners. Shown with logos at campaign page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
