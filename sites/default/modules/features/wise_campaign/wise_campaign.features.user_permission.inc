<?php
/**
 * @file
 * wise_campaign.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wise_campaign_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create funding_partner_logo_s content'.
  $permissions['create funding_partner_logo_s content'] = array(
    'name' => 'create funding_partner_logo_s content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create partner content'.
  $permissions['create partner content'] = array(
    'name' => 'create partner content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create partners_campaign content'.
  $permissions['create partners_campaign content'] = array(
    'name' => 'create partners_campaign content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any funding_partner_logo_s content'.
  $permissions['delete any funding_partner_logo_s content'] = array(
    'name' => 'delete any funding_partner_logo_s content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any partner content'.
  $permissions['delete any partner content'] = array(
    'name' => 'delete any partner content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any partners_campaign content'.
  $permissions['delete any partners_campaign content'] = array(
    'name' => 'delete any partners_campaign content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own funding_partner_logo_s content'.
  $permissions['delete own funding_partner_logo_s content'] = array(
    'name' => 'delete own funding_partner_logo_s content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own partner content'.
  $permissions['delete own partner content'] = array(
    'name' => 'delete own partner content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own partners_campaign content'.
  $permissions['delete own partners_campaign content'] = array(
    'name' => 'delete own partners_campaign content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any funding_partner_logo_s content'.
  $permissions['edit any funding_partner_logo_s content'] = array(
    'name' => 'edit any funding_partner_logo_s content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any partner content'.
  $permissions['edit any partner content'] = array(
    'name' => 'edit any partner content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any partners_campaign content'.
  $permissions['edit any partners_campaign content'] = array(
    'name' => 'edit any partners_campaign content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own funding_partner_logo_s content'.
  $permissions['edit own funding_partner_logo_s content'] = array(
    'name' => 'edit own funding_partner_logo_s content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own partner content'.
  $permissions['edit own partner content'] = array(
    'name' => 'edit own partner content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own partners_campaign content'.
  $permissions['edit own partners_campaign content'] = array(
    'name' => 'edit own partners_campaign content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
