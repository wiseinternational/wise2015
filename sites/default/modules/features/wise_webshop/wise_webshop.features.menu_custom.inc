<?php
/**
 * @file
 * wise_webshop.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function wise_webshop_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-webshop-catalog-menu.
  $menus['menu-webshop-catalog-menu'] = array(
    'menu_name' => 'menu-webshop-catalog-menu',
    'title' => 'Webshop catalog menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Webshop catalog menu');


  return $menus;
}
