<?php
/**
 * @file
 * wise_webshop.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_webshop_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cart';
  $context->description = '';
  $context->tag = 'Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'cart' => 'cart',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-13' => array(
          'module' => 'block',
          'delta' => '13',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Page');
  $export['cart'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'shopping_cart_block';
  $context->description = '';
  $context->tag = 'Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'campaign/cop-21-paris' => 'campaign/cop-21-paris',
        'donate' => 'donate',
        'webshop' => 'webshop',
        'webshop/*' => 'webshop/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'commerce_cart-cart' => array(
          'module' => 'commerce_cart',
          'delta' => 'cart',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Page');
  $export['shopping_cart_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'webshop';
  $context->description = '';
  $context->tag = 'Website section';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'product_display' => 'product_display',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'catalog' => 'catalog',
        'webshop' => 'webshop',
        'webshop/*' => 'webshop/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-webshop-catalog-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-webshop-catalog-menu',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'commerce_cart-cart' => array(
          'module' => 'commerce_cart',
          'delta' => 'cart',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'nodeblock-6328' => array(
          'module' => 'nodeblock',
          'delta' => '6328',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Website section');
  $export['webshop'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'webshop_subpages';
  $context->description = '';
  $context->tag = 'Website section';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'webshop/*' => 'webshop/*',
        'product/*' => 'product/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-13' => array(
          'module' => 'block',
          'delta' => '13',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Website section');
  $export['webshop_subpages'] = $context;

  return $export;
}
