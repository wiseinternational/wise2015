<?php
/**
 * @file
 * wise_webshop.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wise_webshop_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'catalog';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Catalog';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Welcome to our webshop';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '15';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'node';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = 'The Smiling Sun logo was created in 1975 in Denmark and soon after the anti-nuclear power movement in dozens of countries adopted the logo. Within a few years the logo was translated from Danish into some 40 national and regional languages and it rapidly became the most common worldwide symbol in the anti-nuclear power movement. And it still is. The logo is displayed on many different items and has been included in the collection of multiple museums. For more information on the history of the smiling sun visit: www.smilingsun.org

The WISE network was initiated and partially founded by the revenues of the worldwide sales of the Smiling Sun items. It\'s therefore with due pride that we present our re-newed webshop. With familiar and new items for sale!!!

Bulk orders are possible, for instance to re-sell the material so that you help finance the activities of your group.

If you want to sell your material via our website, please contact us  for possibilities. ';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Product display image */
  $handler->display->display_options['fields']['field_product_display_image']['id'] = 'field_product_display_image';
  $handler->display->display_options['fields']['field_product_display_image']['table'] = 'field_data_field_product_display_image';
  $handler->display->display_options['fields']['field_product_display_image']['field'] = 'field_product_display_image';
  $handler->display->display_options['fields']['field_product_display_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_product_display_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_product_display_image']['delta_offset'] = '0';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Contextual filter: Content: Has taxonomy term ID (with depth) */
  $handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
  $handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
  $handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
  $handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['term_node_tid_depth']['summary']['format'] = 'default_summary';
  /* Filter criterion: Content: Product display content type */
  $handler->display->display_options['filters']['product_display_node_type']['id'] = 'product_display_node_type';
  $handler->display->display_options['filters']['product_display_node_type']['table'] = 'node';
  $handler->display->display_options['filters']['product_display_node_type']['field'] = 'product_display_node_type';
  $handler->display->display_options['filters']['product_display_node_type']['value'] = array(
    'product_display' => 'product_display',
  );
  $handler->display->display_options['filters']['product_display_node_type']['group'] = 1;
  /* Filter criterion: Content: Product display image (field_product_display_image:alt) */
  $handler->display->display_options['filters']['field_product_display_image_alt']['id'] = 'field_product_display_image_alt';
  $handler->display->display_options['filters']['field_product_display_image_alt']['table'] = 'field_data_field_product_display_image';
  $handler->display->display_options['filters']['field_product_display_image_alt']['field'] = 'field_product_display_image_alt';
  $handler->display->display_options['filters']['field_product_display_image_alt']['group'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'The profit from our shop goes directly into funding anti-nuclear activities.

You can re-sell the material and help finance the activities of your group. If you want to sell your material via our website, please contact us for possibilities. ';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['path'] = 'webshop';

  /* Display: Page taxonomy */
  $handler = $view->new_display('page', 'Page taxonomy', 'page_1');
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Has taxonomy term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['tid']['default_argument_options']['term_page'] = FALSE;
  $handler->display->display_options['arguments']['tid']['default_argument_options']['node'] = TRUE;
  $handler->display->display_options['arguments']['tid']['default_argument_options']['vocabularies'] = array(
    'webshop' => 'webshop',
  );
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['path'] = 'taxonomy/%/term';
  $export['catalog'] = $view;

  return $export;
}
