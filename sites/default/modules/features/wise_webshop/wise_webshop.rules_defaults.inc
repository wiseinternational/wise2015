<?php
/**
 * @file
 * wise_webshop.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function wise_webshop_default_rules_configuration() {
  $items = array();
  $items['commerce_checkout_send_an_order_notification_e_mail_to_admin'] = entity_import('rules_config', '{ "commerce_checkout_send_an_order_notification_e_mail_to_admin" : {
      "LABEL" : "Send an order notification e-mail to Admin",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "4",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Checkout" ],
      "REQUIRES" : [ "rules", "commerce_checkout" ],
      "ON" : { "commerce_checkout_complete" : [] },
      "DO" : [
        { "mail" : {
            "to" : "thessa@wiseinternational.org",
            "subject" : "Order [commerce-order:order-number] at [site:name]",
            "message" : "Thanks for your order [commerce-order:order-number] at [site:name].\\r\\n\\r\\nIf this is your first order with us, you will receive a separate e-mail with login instructions. You can view your order history with us at any time by logging into our website at:\\r\\n\\r\\n[site:login-url]\\r\\n\\r\\nYou can find the status of your current order at:\\r\\n\\r\\n[commerce-order:customer-url]\\r\\n\\r\\nPlease contact us if you have any questions about your order.",
            "language" : [ "commerce-order:state" ]
          }
        }
      ]
    }
  }');
  $items['commerce_shipping_service_commerce_shipping_field_service'] = entity_import('rules_config', '{ "commerce_shipping_service_commerce_shipping_field_service" : {
      "LABEL" : "Rate Field-Driven Shipping Service",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "commerce_shipping" ],
      "USES VARIABLES" : { "commerce_order" : { "label" : "Order", "type" : "commerce_order" } },
      "DO" : [
        { "commerce_shipping_service_rate_order" : {
            "shipping_service_name" : "large",
            "commerce_order" : [ "commerce-order" ]
          }
        }
      ]
    }
  }');
  $items['rules_redirect_after_add_to_cart'] = entity_import('rules_config', '{ "rules_redirect_after_add_to_cart" : {
      "LABEL" : "Redirect after add to cart",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Cart" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : { "commerce_cart_product_add" : [] },
      "DO" : [ { "redirect" : { "url" : "cart", "force" : "0" } } ]
    }
  }');
  $items['rules_send_order_completion_email'] = entity_import('rules_config', '{ "rules_send_order_completion_email" : {
      "LABEL" : "Order status update",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "php", "entity" ],
      "ON" : { "commerce_order_update" : [] },
      "IF" : [
        { "php_eval" : { "code" : "if(\\u0022[commerce-order:status]\\u0022 == \\u0022completed\\u0022 \\u0026\\u0026 \\u0022[commerce-order-unchanged:status]\\u0022 != \\u0022[commerce-order:status]\\u0022) { return TRUE; } else { return FALSE; }" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "commerce-order:mail" ],
            "subject" : "Order [commerce-order:order-number] at [site:name]",
            "message" : "Your order [commerce-order:order-number] at [site:name] has been completed and has been send to you.\\r\\n\\r\\nThank you for ordering at our shop and supporting the anti-nuclear activities of [site:name].\\r\\n\\r\\nIf you would like to know more please subscribe to our newsletter.\\r\\nat: http:\\/\\/www.wiseinternational.org\\/newsletter\\r\\n\\r\\nAnd of course you can always contact us with further questions.\\r\\n\\r\\n -- Wise.",
            "language" : [ "commerce-order:state" ]
          }
        }
      ]
    }
  }');
  return $items;
}
