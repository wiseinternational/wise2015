<?php
/**
 * @file
 * wise_theme.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_theme_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'green_ball_cop21';
  $context->description = '';
  $context->tag = 'Green ball';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'campaign' => 'campaign',
        'campaign/*' => 'campaign/*',
        '~campaign/sign-petition' => '~campaign/sign-petition',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4315' => array(
          'module' => 'nodeblock',
          'delta' => '4315',
          'region' => 'sidebar_first',
          'weight' => '-35',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Green ball');
  $export['green_ball_cop21'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'green_ball_general';
  $context->description = '';
  $context->tag = 'Green ball';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*' => '*',
        '~map' => '~map',
        '~<front>' => '~<front>',
        '~nuclear-monitor' => '~nuclear-monitor',
        '~nuclear-monitor/*' => '~nuclear-monitor/*',
        '~campaign' => '~campaign',
        '~campaign/*' => '~campaign/*',
        '~cart' => '~cart',
        '~cart/*' => '~cart/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4341' => array(
          'module' => 'nodeblock',
          'delta' => '4341',
          'region' => 'sidebar_first',
          'weight' => '-39',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Green ball');
  $export['green_ball_general'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'green_ball_map';
  $context->description = '';
  $context->tag = 'Green ball';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'map' => 'map',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-6102' => array(
          'module' => 'nodeblock',
          'delta' => '6102',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Green ball');
  $export['green_ball_map'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'homepage';
  $context->description = '';
  $context->tag = 'Website section';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'home' => 'home',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4296' => array(
          'module' => 'nodeblock',
          'delta' => '4296',
          'region' => 'featured_1',
          'weight' => '-10',
        ),
        'nodeblock-4297' => array(
          'module' => 'nodeblock',
          'delta' => '4297',
          'region' => 'featured_2',
          'weight' => '-10',
        ),
        'nodeblock-4298' => array(
          'module' => 'nodeblock',
          'delta' => '4298',
          'region' => 'featured_3',
          'weight' => '-10',
        ),
        'nodeblock-4294' => array(
          'module' => 'nodeblock',
          'delta' => '4294',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
        'block-12' => array(
          'module' => 'block',
          'delta' => '12',
          'region' => 'triptych_first',
          'weight' => '-10',
        ),
        'nodeblock-4310' => array(
          'module' => 'nodeblock',
          'delta' => '4310',
          'region' => 'triptych_middle',
          'weight' => '-10',
        ),
        'nodeblock-4299' => array(
          'module' => 'nodeblock',
          'delta' => '4299',
          'region' => 'triptych_last',
          'weight' => '-10',
        ),
        'views-bestsellers-block_1' => array(
          'module' => 'views',
          'delta' => 'bestsellers-block_1',
          'region' => 'vm_slider_2',
          'weight' => '-10',
        ),
        'block-7' => array(
          'module' => 'block',
          'delta' => '7',
          'region' => 'triptych2_first',
          'weight' => '-10',
        ),
        'nodeblock-4309' => array(
          'module' => 'nodeblock',
          'delta' => '4309',
          'region' => 'triptych2_middle',
          'weight' => '-10',
        ),
        'nodeblock-4300' => array(
          'module' => 'nodeblock',
          'delta' => '4300',
          'region' => 'triptych2_last',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Website section');
  $export['homepage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'homepage_map';
  $context->description = '';
  $context->tag = 'Map';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-14' => array(
          'module' => 'block',
          'delta' => '14',
          'region' => 'fullwidth_featured',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Map');
  $export['homepage_map'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'niet_home';
  $context->description = 'Top afbeelding algemeen';
  $context->tag = 'Website section';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
        '~campaign' => '~campaign',
        '~campaign/*' => '~campaign/*',
        '~nuclear-energy' => '~nuclear-energy',
        '~nuclear-energy/*' => '~nuclear-energy/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4295' => array(
          'module' => 'nodeblock',
          'delta' => '4295',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Top afbeelding algemeen');
  t('Website section');
  $export['niet_home'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = '';
  $context->tag = 'Website section';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'commerce_cart-cart' => array(
          'module' => 'commerce_cart',
          'delta' => 'cart',
          'region' => 'cart',
          'weight' => '-10',
        ),
        'custom_search_blocks-1' => array(
          'module' => 'custom_search_blocks',
          'delta' => '1',
          'region' => 'search_block',
          'weight' => '-10',
        ),
        'menu-menu-about-wise-footer' => array(
          'module' => 'menu',
          'delta' => 'menu-about-wise-footer',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'menu-menu-nuclear-energy-' => array(
          'module' => 'menu',
          'delta' => 'menu-nuclear-energy-',
          'region' => 'footer',
          'weight' => '-9',
        ),
        'menu-menu-take-action-footer' => array(
          'module' => 'menu',
          'delta' => 'menu-take-action-footer',
          'region' => 'footer',
          'weight' => '-8',
        ),
        'views-partners-block' => array(
          'module' => 'views',
          'delta' => 'partners-block',
          'region' => 'partners',
          'weight' => '-10',
        ),
        'menu-menu-footer-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-footer-menu',
          'region' => 'below_footer',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Website section');
  $export['sitewide'] = $context;

  return $export;
}
