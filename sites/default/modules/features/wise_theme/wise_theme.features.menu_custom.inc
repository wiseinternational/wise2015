<?php
/**
 * @file
 * wise_theme.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function wise_theme_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-about-wise-footer.
  $menus['menu-about-wise-footer'] = array(
    'menu_name' => 'menu-about-wise-footer',
    'title' => 'About WISE footer',
    'description' => '',
  );
  // Exported menu: menu-footer-menu.
  $menus['menu-footer-menu'] = array(
    'menu_name' => 'menu-footer-menu',
    'title' => 'Footer menu',
    'description' => '',
  );
  // Exported menu: menu-take-action-footer.
  $menus['menu-take-action-footer'] = array(
    'menu_name' => 'menu-take-action-footer',
    'title' => 'Take action footer',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About WISE footer');
  t('Footer menu');
  t('Take action footer');


  return $menus;
}
