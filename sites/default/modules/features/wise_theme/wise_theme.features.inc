<?php
/**
 * @file
 * wise_theme.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_theme_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wise_theme_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wise_theme_node_info() {
  $items = array(
    'home_background_image' => array(
      'name' => t('Home background image'),
      'base' => 'node_content',
      'description' => t('De afbeelding in de header van de homepage.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'nodeblock_rechterkolom' => array(
      'name' => t('NodeBlock rechterkolom'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'vervolgpagina_background_image' => array(
      'name' => t('Vervolgpagina background image'),
      'base' => 'node_content',
      'description' => t('De afbeelding in de header van alle pagina\'s behalve de voorpagina.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
