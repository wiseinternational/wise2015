<?php
/**
 * @file
 * wise_map.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function wise_map_user_default_roles() {
  $roles = array();

  // Exported role: contributor.
  $roles['contributor'] = array(
    'name' => 'contributor',
    'weight' => 3,
  );

  return $roles;
}
