<?php
/**
 * @file
 * wise_map.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function wise_map_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'location_import';
  $feeds_importer->config = array(
    'name' => 'Location import',
    'description' => 'Imports location nodes from a CSV file',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          0 => 'public',
          1 => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'location',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Latitude',
            'target' => 'field_location:lat',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Longitude',
            'target' => 'field_location:lon',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Type',
            'target' => 'field_type',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Alternative name',
            'target' => 'field_alternative_name',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Tags',
            'target' => 'field_tags',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          6 => array(
            'source' => 'Owner',
            'target' => 'field_owner',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Operator',
            'target' => 'field_operator',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Status',
            'target' => 'field_status',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'Opening year',
            'target' => 'field_opening_year',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'Closing year',
            'target' => 'field_closing_year',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'Link',
            'target' => 'field_links:url',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'Comment',
            'target' => 'field_comment',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'Wise office',
            'target' => 'field_wise_office',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'Local',
            'target' => 'field_work_area',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'Regional',
            'target' => 'field_work_area',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'National',
            'target' => 'field_work_area',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'International',
            'target' => 'field_work_area',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'Email',
            'target' => 'field_email',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'Campaign topic 1',
            'target' => 'field_campaign',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'Campaign topic 2',
            'target' => 'field_campaign',
            'unique' => FALSE,
          ),
          21 => array(
            'source' => 'Campaign topic 3',
            'target' => 'field_campaign',
            'unique' => FALSE,
          ),
          22 => array(
            'source' => 'Nuclear installation type',
            'target' => 'field_nuclear_installation_type',
            'unique' => FALSE,
          ),
          23 => array(
            'source' => 'Capacity',
            'target' => 'field_capacity',
            'unique' => FALSE,
          ),
          24 => array(
            'source' => 'Utility',
            'target' => 'field_utility',
            'unique' => FALSE,
          ),
          25 => array(
            'source' => 'Fuel',
            'target' => 'field_fuel',
            'unique' => FALSE,
          ),
          26 => array(
            'source' => 'Enrichment technique',
            'target' => 'field_enrichment_technique',
            'unique' => FALSE,
          ),
          27 => array(
            'source' => 'SWU',
            'target' => 'field_swu',
            'unique' => FALSE,
          ),
          28 => array(
            'source' => 'Reprocessing technique',
            'target' => 'field_reprocessing_technique',
            'unique' => FALSE,
          ),
          29 => array(
            'source' => 'Uranium capacity',
            'target' => 'field_uranium_capacity',
            'unique' => FALSE,
          ),
          30 => array(
            'source' => 'Plutonium capacity',
            'target' => 'field_plutonium_capacity',
            'unique' => FALSE,
          ),
          31 => array(
            'source' => 'Production',
            'target' => 'field_production',
            'unique' => FALSE,
          ),
          32 => array(
            'source' => 'Share of world production',
            'target' => 'field_share_of_world_production',
            'unique' => FALSE,
          ),
          33 => array(
            'source' => 'Mining technique',
            'target' => 'field_mining_technique',
            'unique' => FALSE,
          ),
          34 => array(
            'source' => 'LLW',
            'target' => 'field_waste',
            'unique' => FALSE,
          ),
          35 => array(
            'source' => 'ILW',
            'target' => 'field_waste',
            'unique' => FALSE,
          ),
          36 => array(
            'source' => 'HLW',
            'target' => 'field_waste',
            'unique' => FALSE,
          ),
          37 => array(
            'source' => 'Storage method',
            'target' => 'field_storage_method',
            'unique' => FALSE,
          ),
          38 => array(
            'source' => 'Culprit',
            'target' => 'field_culprit',
            'unique' => FALSE,
          ),
          39 => array(
            'source' => 'What was dumped',
            'target' => 'field_dumped',
            'unique' => FALSE,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => '109',
        'authorize' => 0,
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['location_import'] = $feeds_importer;

  return $export;
}
