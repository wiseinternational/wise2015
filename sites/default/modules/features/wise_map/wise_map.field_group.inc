<?php
/**
 * @file
 * wise_map.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function wise_map_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dumpsite|node|location|form';
  $field_group->group_name = 'group_dumpsite';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Dump site',
    'weight' => '11',
    'children' => array(
      0 => 'field_culprit',
      1 => 'field_dumped',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_dumpsite|node|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_enrichment|node|location|form';
  $field_group->group_name = 'group_enrichment';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Enrichment facility',
    'weight' => '7',
    'children' => array(
      0 => 'field_enrichment_technique',
      1 => 'field_swu',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-enrichment field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_enrichment|node|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general|node|location|form';
  $field_group->group_name = 'group_general';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'General',
    'weight' => '4',
    'children' => array(
      0 => 'field_alternative_name',
      1 => 'field_links',
      2 => 'field_tags',
      3 => 'field_owner',
      4 => 'field_comment',
      5 => 'field_status',
      6 => 'field_closing_year',
      7 => 'field_operator',
      8 => 'field_opening_year',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-general field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_general|node|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hotspot|node|location|form';
  $field_group->group_name = 'group_hotspot';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hot spots & Dumping sites',
    'weight' => '12',
    'children' => array(
      0 => 'field_what_happened_here',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-hotspot field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_hotspot|node|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_mine|node|location|form';
  $field_group->group_name = 'group_mine';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Uranium mine',
    'weight' => '9',
    'children' => array(
      0 => 'field_production',
      1 => 'field_share_of_world_production',
      2 => 'field_mining_technique',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Uranium mine',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-mine field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_mine|node|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_organisation|node|location|form';
  $field_group->group_name = 'group_organisation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Organisation',
    'weight' => '5',
    'children' => array(
      0 => 'field_wise_office',
      1 => 'field_email',
      2 => 'field_work_area',
      3 => 'field_campaign',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_organisation|node|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_plant|node|location|form';
  $field_group->group_name = 'group_plant';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Nuclear plant',
    'weight' => '6',
    'children' => array(
      0 => 'field_utility',
      1 => 'field_capacity',
      2 => 'field_nuclear_installation_type',
      3 => 'field_fuel',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Nuclear plant',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-plant field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_plant|node|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_reprocessing|node|location|form';
  $field_group->group_name = 'group_reprocessing';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Reproecssing facility',
    'weight' => '8',
    'children' => array(
      0 => 'field_reprocessing_technique',
      1 => 'field_uranium_capacity',
      2 => 'field_plutonium_capacity',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-reprocessing field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_reprocessing|node|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_route|node|location|form';
  $field_group->group_name = 'group_route';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Transport route',
    'weight' => '13',
    'children' => array(
      0 => 'field_transport_type',
      1 => 'field_transports',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-route field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_route|node|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_waste|node|location|form';
  $field_group->group_name = 'group_waste';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Waste facility',
    'weight' => '10',
    'children' => array(
      0 => 'field_storage_method',
      1 => 'field_waste',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-waste field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_waste|node|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_weapons|node|location|form';
  $field_group->group_name = 'group_weapons';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Weapons',
    'weight' => '11',
    'children' => array(
      0 => 'field_amount_of_weapons',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-weapons field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_weapons|node|location|form'] = $field_group;

  return $export;
}
