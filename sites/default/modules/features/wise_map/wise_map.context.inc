<?php
/**
 * @file
 * wise_map.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_map_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'location';
  $context->description = 'Temporary - Map of indexed locations with faceted search';
  $context->tag = 'Map';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'map' => 'map',
        'map/*' => 'map/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'facetapi-iRpteUmcnpITpfnvgpdckFF9C80p9ulm' => array(
          'module' => 'facetapi',
          'delta' => 'iRpteUmcnpITpfnvgpdckFF9C80p9ulm',
          'region' => 'above_content',
          'weight' => '-10',
        ),
        'facetapi-q5avPwOsCVPjepS651nhRiTjmJjFo1Ae' => array(
          'module' => 'facetapi',
          'delta' => 'q5avPwOsCVPjepS651nhRiTjmJjFo1Ae',
          'region' => 'above_content',
          'weight' => '-9',
        ),
        'facetapi-r0YMCGBWUuVUNLeUGcflZ1bS6DoCd7SO' => array(
          'module' => 'facetapi',
          'delta' => 'r0YMCGBWUuVUNLeUGcflZ1bS6DoCd7SO',
          'region' => 'above_content',
          'weight' => '-8',
        ),
        'facetapi-SS5t8a75F5UPa8kyIe77CB8fsDNgcXu4' => array(
          'module' => 'facetapi',
          'delta' => 'SS5t8a75F5UPa8kyIe77CB8fsDNgcXu4',
          'region' => 'above_content',
          'weight' => '-7',
        ),
        'facetapi-Jf70x5eWHsfvq0ncsXmW153WGbNrdJ4u' => array(
          'module' => 'facetapi',
          'delta' => 'Jf70x5eWHsfvq0ncsXmW153WGbNrdJ4u',
          'region' => 'above_content',
          'weight' => '-6',
        ),
        'facetapi-uC5YIUPB6ildNNuuLSWWDm6n7AWG0z0Y' => array(
          'module' => 'facetapi',
          'delta' => 'uC5YIUPB6ildNNuuLSWWDm6n7AWG0z0Y',
          'region' => 'above_content',
          'weight' => '-5',
        ),
        'facetapi-exlmsjCvW0Lji4XdtJ5KGYgJeMUK9LjY' => array(
          'module' => 'facetapi',
          'delta' => 'exlmsjCvW0Lji4XdtJ5KGYgJeMUK9LjY',
          'region' => 'above_content',
          'weight' => '-4',
        ),
        'facetapi-0iL27uDWSP6HXpgs4Wr9wrv98nrC6TYg' => array(
          'module' => 'facetapi',
          'delta' => '0iL27uDWSP6HXpgs4Wr9wrv98nrC6TYg',
          'region' => 'above_content',
          'weight' => '-3',
        ),
        'facetapi-C1ytb1F6weXiWWT1288eDXvT53SdXLMl' => array(
          'module' => 'facetapi',
          'delta' => 'C1ytb1F6weXiWWT1288eDXvT53SdXLMl',
          'region' => 'above_content',
          'weight' => '-2',
        ),
        'facetapi-KfqyPwQqvBYW029ZxR114DmiXBLeHeg8' => array(
          'module' => 'facetapi',
          'delta' => 'KfqyPwQqvBYW029ZxR114DmiXBLeHeg8',
          'region' => 'above_content',
          'weight' => '-1',
        ),
        'views--exp-location_search-page_1' => array(
          'module' => 'views',
          'delta' => '-exp-location_search-page_1',
          'region' => 'above_content',
          'weight' => '0',
        ),
        'block-11' => array(
          'module' => 'block',
          'delta' => '11',
          'region' => 'above_content',
          'weight' => '1',
        ),
        'block-9' => array(
          'module' => 'block',
          'delta' => '9',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-location_type_factsheets-block' => array(
          'module' => 'views',
          'delta' => 'location_type_factsheets-block',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Map');
  t('Temporary - Map of indexed locations with faceted search');
  $export['location'] = $context;

  return $export;
}
