<?php
/**
 * @file
 * wise_map.openlayers_styles.inc
 */

/**
 * Implements hook_openlayers_styles().
 */
function wise_map_openlayers_styles() {
  $export = array();

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'wise_marker';
  $openlayers_styles->title = 'wise marker';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wise/images/markers/research-facility.png',
    'pointRadius' => 16,
    'fillColor' => '#EE9900',
    'fillOpacity' => 1,
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['wise_marker'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'wise_organisation';
  $openlayers_styles->title = 'wise organisation';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wise/images/markers/organisation.png',
    'pointRadius' => 16,
    'fillColor' => '#EE9900',
    'fillOpacity' => 1,
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['wise_organisation'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'wise_power_station';
  $openlayers_styles->title = 'Wise power station';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wise/images/markers/power-station.png',
    'pointRadius' => 16,
    'fillColor' => '#EE9900',
    'fillOpacity' => 1,
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['wise_power_station'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'wise_uraniume_mine';
  $openlayers_styles->title = 'Wise uranium mine';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wise/images/markers/uranium-mine.png',
    'pointRadius' => 16,
    'fillColor' => '#EE9900',
    'fillOpacity' => 1,
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['wise_uraniume_mine'] = $openlayers_styles;

  return $export;
}
