<?php
/**
 * @file
 * wise_map.openlayers_maps.inc
 */

/**
 * Implements hook_openlayers_maps().
 */
function wise_map_openlayers_maps() {
  $export = array();

  $openlayers_maps = new stdClass();
  $openlayers_maps->disabled = FALSE; /* Edit this to true to make a default openlayers_maps disabled initially */
  $openlayers_maps->api_version = 1;
  $openlayers_maps->name = 'locations_all';
  $openlayers_maps->title = 'Locations: all';
  $openlayers_maps->description = 'All locations: groups, places and incidents';
  $openlayers_maps->data = array(
    'width' => 'auto',
    'height' => '600px',
    'image_path' => 'sites/all/modules/openlayers/themes/default_dark/img/',
    'css_path' => 'sites/all/modules/openlayers/themes/default_dark/style.css',
    'proxy_host' => '',
    'hide_empty_map' => 0,
    'center' => array(
      'initial' => array(
        'centerpoint' => '-7.186510064978123e-9, 16.636191882387134',
        'zoom' => '2',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'behaviors' => array(
      'openlayers_behavior_cluster' => array(
        'clusterlayer' => array(
          'location_search_layers_openlayers_5' => 'location_search_layers_openlayers_5',
          'location_search_layers_openlayers_6' => 'location_search_layers_openlayers_6',
          'location_search_layers_openlayers_4' => 'location_search_layers_openlayers_4',
          'location_search_layers_openlayers_3' => 'location_search_layers_openlayers_3',
        ),
        'distance' => '20',
        'threshold' => '',
        'display_cluster_numbers' => 1,
        'middle_lower_bound' => '15',
        'middle_upper_bound' => '50',
        'low_color' => 'rgb(141, 203, 61)',
        'low_stroke_color' => 'rgb(141, 203, 61)',
        'low_opacity' => '0.8',
        'low_point_radius' => '10',
        'low_label_outline' => '1',
        'middle_color' => 'rgb(49, 190, 145)',
        'middle_stroke_color' => 'rgb(49, 190, 145)',
        'middle_opacity' => '0.8',
        'middle_point_radius' => '16',
        'middle_label_outline' => '1',
        'high_color' => 'rgb(35, 59, 177)',
        'high_stroke_color' => 'rgb(35, 59, 177)',
        'high_opacity' => '0.8',
        'high_point_radius' => '22',
        'high_label_outline' => '1',
        'label_low_color' => '#000000',
        'label_low_opacity' => '0.8',
        'label_middle_color' => '#000000',
        'label_middle_opacity' => '0.8',
        'label_high_color' => '#000000',
        'label_high_opacity' => '0.8',
      ),
      'openlayers_behavior_attribution' => array(
        'separator' => '',
      ),
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 0,
        'zoomBoxEnabled' => 1,
        'documentDrag' => 0,
      ),
      'openlayers_behavior_panzoombar' => array(
        'zoomWorldIcon' => 1,
        'panIcons' => 1,
      ),
      'openlayers_behavior_popup' => array(
        'layers' => array(
          'location_search_layers_openlayers_5' => 'location_search_layers_openlayers_5',
          'location_search_layers_openlayers_6' => 'location_search_layers_openlayers_6',
          'location_search_layers_openlayers_4' => 'location_search_layers_openlayers_4',
          'location_search_layers_openlayers_3' => 'location_search_layers_openlayers_3',
        ),
        'popupAtPosition' => 'mouse',
        'panMapIfOutOfView' => 0,
        'keepInMap' => 1,
      ),
      'openlayers_behavior_zoomtolayer' => array(
        'zoomtolayer' => array(
          'location_search_layers_openlayers_5' => 'location_search_layers_openlayers_5',
          'location_search_layers_openlayers_6' => 'location_search_layers_openlayers_6',
          'location_search_layers_openlayers_4' => 'location_search_layers_openlayers_4',
          'location_search_layers_openlayers_3' => 'location_search_layers_openlayers_3',
          'osm_mapnik' => 0,
        ),
        'point_zoom_level' => '5',
        'zoomtolayer_scale' => '1',
      ),
    ),
    'default_layer' => 'osm_mapnik',
    'layers' => array(
      'osm_mapnik' => 'osm_mapnik',
      'location_search_layers_openlayers_5' => 'location_search_layers_openlayers_5',
      'location_search_layers_openlayers_6' => 'location_search_layers_openlayers_6',
      'location_search_layers_openlayers_4' => 'location_search_layers_openlayers_4',
      'location_search_layers_openlayers_3' => 'location_search_layers_openlayers_3',
      'location_search_layers_openlayers_1' => 'location_search_layers_openlayers_1',
    ),
    'layer_weight' => array(
      'geofield_formatter' => '0',
      'location_search_openlayers_1' => '0',
      'location_search_openlayers_2' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'location_search_layers_openlayers_5' => '1',
      'location_search_layers_openlayers_6' => '5',
      'location_search_layers_openlayers_4' => '6',
      'location_search_layers_openlayers_3' => '7',
      'location_search_layers_openlayers_1' => '10',
    ),
    'layer_styles' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'location_search_openlayers_1' => '0',
      'location_search_openlayers_2' => '0',
      'location_search_layers_openlayers_1' => 'invisible',
      'location_search_layers_openlayers_5' => 'wise_uraniume_mine',
      'location_search_layers_openlayers_6' => 'wise_marker',
      'location_search_layers_openlayers_4' => 'wise_power_station',
      'location_search_layers_openlayers_3' => 'wise_organisation',
    ),
    'layer_styles_select' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'location_search_openlayers_1' => '0',
      'location_search_openlayers_2' => '0',
      'location_search_layers_openlayers_1' => 'invisible',
      'location_search_layers_openlayers_5' => 'wise_uraniume_mine',
      'location_search_layers_openlayers_6' => 'wise_marker',
      'location_search_layers_openlayers_4' => 'wise_marker',
      'location_search_layers_openlayers_3' => '0',
    ),
    'layer_styles_temporary' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'location_search_openlayers_1' => '0',
      'location_search_openlayers_2' => '0',
      'location_search_layers_openlayers_1' => 'invisible',
      'location_search_layers_openlayers_5' => 'wise_uraniume_mine',
      'location_search_layers_openlayers_6' => 'wise_marker',
      'location_search_layers_openlayers_4' => 'wise_marker',
      'location_search_layers_openlayers_3' => '0',
    ),
    'layer_activated' => array(
      'location_search_layers_openlayers_1' => 'location_search_layers_openlayers_1',
      'location_search_layers_openlayers_5' => 'location_search_layers_openlayers_5',
      'location_search_layers_openlayers_6' => 'location_search_layers_openlayers_6',
      'location_search_layers_openlayers_4' => 'location_search_layers_openlayers_4',
      'location_search_layers_openlayers_3' => 'location_search_layers_openlayers_3',
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
      'location_search_openlayers_1' => 0,
      'location_search_openlayers_2' => 0,
    ),
    'layer_switcher' => array(
      'location_search_layers_openlayers_5' => 0,
      'location_search_layers_openlayers_6' => 0,
      'location_search_layers_openlayers_4' => 0,
      'location_search_layers_openlayers_3' => 0,
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
      'location_search_openlayers_1' => 0,
      'location_search_openlayers_2' => 0,
      'location_search_layers_openlayers_1' => 0,
    ),
    'projection' => 'EPSG:3857',
    'displayProjection' => 'EPSG:4326',
    'styles' => array(
      'default' => 'default_marker_black',
      'select' => 'default_marker_black_small',
      'temporary' => 'default_marker_black_small',
    ),
  );
  $export['locations_all'] = $openlayers_maps;

  return $export;
}
