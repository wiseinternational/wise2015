<?php
/**
 * @file
 * wise_map.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function wise_map_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Reprocessing facility',
    'description' => '',
    'format' => 'rich_text_editor',
    'weight' => 6,
    'uuid' => '0720e8dc-34a0-4d1f-9cc3-cd454dee5b67',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(),
  );
  $terms[] = array(
    'name' => 'Nuclear power station',
    'description' => '',
    'format' => 'rich_text_editor',
    'weight' => 2,
    'uuid' => '180caf26-29e6-40db-a805-c92b25176890',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(),
  );
  $terms[] = array(
    'name' => 'Evacuation zone',
    'description' => '',
    'format' => 'rich_text_editor',
    'weight' => 13,
    'uuid' => '1a5c9b5a-23d1-485b-8373-1784aa059229',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(),
  );
  $terms[] = array(
    'name' => 'Enrichment facility',
    'description' => '',
    'format' => 'full_html',
    'weight' => 5,
    'uuid' => '6be671ab-022a-43f1-b399-d8a013e206ac',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(),
  );
  $terms[] = array(
    'name' => 'Transport route',
    'description' => '',
    'format' => 'full_html',
    'weight' => 12,
    'uuid' => '8e98eb00-32a7-4db3-b5d6-09f07af9924c',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(),
  );
  $terms[] = array(
    'name' => 'Nuclear submarine',
    'description' => '',
    'format' => 'rich_text_editor',
    'weight' => 4,
    'uuid' => '9842bcd0-5444-40b4-becc-01b4c9a8f18f',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(),
  );
  $terms[] = array(
    'name' => 'Nuclear research facility',
    'description' => '',
    'format' => 'rich_text_editor',
    'weight' => 3,
    'uuid' => '99d107a8-5685-4bb1-aa83-a2849b0fa3d3',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(),
  );
  $terms[] = array(
    'name' => 'Nuclear weapons',
    'description' => '',
    'format' => 'rich_text_editor',
    'weight' => 10,
    'uuid' => '9eb1845a-a1ca-451b-ac30-00fe8b29c18b',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(),
  );
  $terms[] = array(
    'name' => 'Dumpsites',
    'description' => '',
    'format' => 'rich_text_editor',
    'weight' => 11,
    'uuid' => 'c0dc8e68-197c-41b8-8d4e-13c7839a4b45',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(),
  );
  $terms[] = array(
    'name' => 'Waste facility',
    'description' => '',
    'format' => 'rich_text_editor',
    'weight' => 9,
    'uuid' => 'd20a7010-79e5-4890-81ee-60efa870ad7b',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(
      'und' => array(
        0 => array(
          'target_id' => 'f31a314f-ccf7-4dd1-a2fa-29dab2335b17',
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Organisation',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'd3ea8ede-ae87-4a51-acae-f94197d2ba37',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(),
  );
  $terms[] = array(
    'name' => 'Uranium mine',
    'description' => '',
    'format' => 'full_html',
    'weight' => 7,
    'uuid' => 'd9133035-5319-4c87-8430-2c05d046a16f',
    'vocabulary_machine_name' => 'type',
    'field_factsheet' => array(
      'und' => array(
        0 => array(
          'target_id' => 'e2159282-d124-4e61-924c-2d5cfd948be4',
        ),
      ),
    ),
  );
  return $terms;
}
