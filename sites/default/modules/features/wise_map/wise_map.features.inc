<?php
/**
 * @file
 * wise_map.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_map_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "openlayers" && $api == "openlayers_maps") {
    return array("version" => "1");
  }
  if ($module == "openlayers" && $api == "openlayers_styles") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wise_map_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wise_map_node_info() {
  $items = array(
    'location' => array(
      'name' => t('Location'),
      'base' => 'node_content',
      'description' => t('anything that can be placed on a map'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function wise_map_default_search_api_index() {
  $items = array();
  $items['location_index'] = entity_import('search_api_index', '{
    "name" : "Location index",
    "machine_name" : "location_index",
    "description" : null,
    "server" : "database",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "field_alternative_name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_campaign" : { "type" : "list\\u003Ctext\\u003E" },
        "field_capacity" : { "type" : "integer", "boost" : "0.5" },
        "field_closing_year" : { "type" : "integer" },
        "field_comment" : { "type" : "text", "boost" : "0.8" },
        "field_culprit" : { "type" : "text", "boost" : "8.0" },
        "field_dumped" : { "type" : "text", "boost" : "8.0" },
        "field_email" : { "type" : "text", "boost" : "0.3" },
        "field_enrichment_technique" : { "type" : "string" },
        "field_fuel" : { "type" : "string" },
        "field_mining_technique" : { "type" : "string" },
        "field_nuclear_installation_type" : { "type" : "string" },
        "field_opening_year" : { "type" : "date" },
        "field_operator" : { "type" : "string" },
        "field_owner" : { "type" : "string" },
        "field_plutonium_capacity" : { "type" : "integer" },
        "field_production" : { "type" : "integer" },
        "field_reprocessing_technique" : { "type" : "string" },
        "field_share_of_world_production" : { "type" : "integer" },
        "field_status" : { "type" : "string" },
        "field_storage_method" : { "type" : "string" },
        "field_swu" : { "type" : "decimal" },
        "field_tags" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_uranium_capacity" : { "type" : "integer" },
        "field_utility" : { "type" : "string", "boost" : "0.8" },
        "field_waste" : { "type" : "list\\u003Cstring\\u003E" },
        "field_wise_office" : { "type" : "string" },
        "field_work_area" : { "type" : "list\\u003Cstring\\u003E" },
        "nid" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "title" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "location" : "location" } }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "field_type:parent" : "field_type:parent" } }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true, "field_alternative_name" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "field_alternative_name" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 1,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "field_alternative_name" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}
