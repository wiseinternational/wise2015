<?php
/**
 * @file
 * wise_administration_configuration.backup_migrate_exportables.inc
 */

/**
 * Implements hook_exportables_backup_migrate_destinations().
 */
function wise_administration_configuration_exportables_backup_migrate_destinations() {
  $export = array();

  $item = new stdClass();
  $item->disabled = FALSE; /* Edit this to true to make a default item disabled initially */
  $item->api_version = 1;
  $item->machine_name = 'manual';
  $item->name = 'Manual Backups Directory';
  $item->subtype = 'file_manual';
  $item->location = '../backups/wise/manual';
  $item->settings = array(
    'chmod' => '',
    'chgrp' => '',
  );
  $export['manual'] = $item;

  $item = new stdClass();
  $item->disabled = FALSE; /* Edit this to true to make a default item disabled initially */
  $item->api_version = 1;
  $item->machine_name = 'scheduled';
  $item->name = 'Scheduled Backups Directory';
  $item->subtype = 'file_scheduled';
  $item->location = '../backups/wise/scheduled';
  $item->settings = array(
    'chmod' => '',
    'chgrp' => '',
  );
  $export['scheduled'] = $item;

  return $export;
}
