<?php
/**
 * @file
 * wise_petition.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function wise_petition_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['nodeblock-4898'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 4898,
    'module' => 'nodeblock',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-manage_registrations-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'manage_registrations-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-petitions-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'petitions-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'Choose a language',
    'visibility' => 0,
  );

  $export['views-signed_petitions-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'signed_petitions-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wise' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise',
        'weight' => 0,
      ),
      'wise_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wise_theme',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
