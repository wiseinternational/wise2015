<?php
/**
 * @file
 * wise_petition.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_petition_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wise_petition_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wise_petition_node_info() {
  $items = array(
    'petition' => array(
      'name' => t('Petition'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_registration_type().
 */
function wise_petition_default_registration_type() {
  $items = array();
  $items['petition'] = entity_import('registration_type', '{
    "name" : "petition",
    "label" : "Petition (french)",
    "locked" : "0",
    "default_state" : null,
    "weight" : "0",
    "held_expire" : "1",
    "held_expire_state" : "canceled",
    "rdf_mapping" : []
  }');
  $items['petition_espana_'] = entity_import('registration_type', '{
    "name" : "petition_espana_",
    "label" : "Petition (espana)",
    "locked" : "0",
    "default_state" : null,
    "weight" : "0",
    "held_expire" : "1",
    "held_expire_state" : "canceled",
    "rdf_mapping" : []
  }');
  $items['petition_french_cloned_'] = entity_import('registration_type', '{
    "name" : "petition_french_cloned_",
    "label" : "Petition (english)",
    "locked" : "0",
    "default_state" : null,
    "weight" : "0",
    "held_expire" : "1",
    "held_expire_state" : "canceled",
    "rdf_mapping" : []
  }');
  $items['petition_german_'] = entity_import('registration_type', '{
    "name" : "petition_german_",
    "label" : "Petition (german)",
    "locked" : "0",
    "default_state" : null,
    "weight" : "0",
    "held_expire" : "1",
    "held_expire_state" : "canceled",
    "rdf_mapping" : []
  }');
  return $items;
}
