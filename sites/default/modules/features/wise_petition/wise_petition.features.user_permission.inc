<?php
/**
 * @file
 * wise_petition.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wise_petition_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer own petition registration'.
  $permissions['administer own petition registration'] = array(
    'name' => 'administer own petition registration',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer own petition_espana_ registration'.
  $permissions['administer own petition_espana_ registration'] = array(
    'name' => 'administer own petition_espana_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer own petition_french_cloned_ registration'.
  $permissions['administer own petition_french_cloned_ registration'] = array(
    'name' => 'administer own petition_french_cloned_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer own petition_german_ registration'.
  $permissions['administer own petition_german_ registration'] = array(
    'name' => 'administer own petition_german_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer petition registration'.
  $permissions['administer petition registration'] = array(
    'name' => 'administer petition registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer petition_espana_ registration'.
  $permissions['administer petition_espana_ registration'] = array(
    'name' => 'administer petition_espana_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer petition_french_cloned_ registration'.
  $permissions['administer petition_french_cloned_ registration'] = array(
    'name' => 'administer petition_french_cloned_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer petition_german_ registration'.
  $permissions['administer petition_german_ registration'] = array(
    'name' => 'administer petition_german_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer registration'.
  $permissions['administer registration'] = array(
    'name' => 'administer registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer registration states'.
  $permissions['administer registration states'] = array(
    'name' => 'administer registration states',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer registration types'.
  $permissions['administer registration types'] = array(
    'name' => 'administer registration types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition content'.
  $permissions['create petition content'] = array(
    'name' => 'create petition content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create petition registration'.
  $permissions['create petition registration'] = array(
    'name' => 'create petition registration',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition registration other anonymous'.
  $permissions['create petition registration other anonymous'] = array(
    'name' => 'create petition registration other anonymous',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition registration other users'.
  $permissions['create petition registration other users'] = array(
    'name' => 'create petition registration other users',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition_espana_ registration'.
  $permissions['create petition_espana_ registration'] = array(
    'name' => 'create petition_espana_ registration',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition_espana_ registration other anonymous'.
  $permissions['create petition_espana_ registration other anonymous'] = array(
    'name' => 'create petition_espana_ registration other anonymous',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition_espana_ registration other users'.
  $permissions['create petition_espana_ registration other users'] = array(
    'name' => 'create petition_espana_ registration other users',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition_french_cloned_ registration'.
  $permissions['create petition_french_cloned_ registration'] = array(
    'name' => 'create petition_french_cloned_ registration',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition_french_cloned_ registration other anonymous'.
  $permissions['create petition_french_cloned_ registration other anonymous'] = array(
    'name' => 'create petition_french_cloned_ registration other anonymous',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition_french_cloned_ registration other users'.
  $permissions['create petition_french_cloned_ registration other users'] = array(
    'name' => 'create petition_french_cloned_ registration other users',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition_german_ registration'.
  $permissions['create petition_german_ registration'] = array(
    'name' => 'create petition_german_ registration',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition_german_ registration other anonymous'.
  $permissions['create petition_german_ registration other anonymous'] = array(
    'name' => 'create petition_german_ registration other anonymous',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create petition_german_ registration other users'.
  $permissions['create petition_german_ registration other users'] = array(
    'name' => 'create petition_german_ registration other users',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'delete any petition content'.
  $permissions['delete any petition content'] = array(
    'name' => 'delete any petition content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any petition registration'.
  $permissions['delete any petition registration'] = array(
    'name' => 'delete any petition registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'delete any petition_espana_ registration'.
  $permissions['delete any petition_espana_ registration'] = array(
    'name' => 'delete any petition_espana_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'delete any petition_french_cloned_ registration'.
  $permissions['delete any petition_french_cloned_ registration'] = array(
    'name' => 'delete any petition_french_cloned_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'delete any petition_german_ registration'.
  $permissions['delete any petition_german_ registration'] = array(
    'name' => 'delete any petition_german_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'delete own petition content'.
  $permissions['delete own petition content'] = array(
    'name' => 'delete own petition content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own petition registration'.
  $permissions['delete own petition registration'] = array(
    'name' => 'delete own petition registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'delete own petition_espana_ registration'.
  $permissions['delete own petition_espana_ registration'] = array(
    'name' => 'delete own petition_espana_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'delete own petition_french_cloned_ registration'.
  $permissions['delete own petition_french_cloned_ registration'] = array(
    'name' => 'delete own petition_french_cloned_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'delete own petition_german_ registration'.
  $permissions['delete own petition_german_ registration'] = array(
    'name' => 'delete own petition_german_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'edit any petition content'.
  $permissions['edit any petition content'] = array(
    'name' => 'edit any petition content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own petition content'.
  $permissions['edit own petition content'] = array(
    'name' => 'edit own petition content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit petition registration state'.
  $permissions['edit petition registration state'] = array(
    'name' => 'edit petition registration state',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'edit petition_espana_ registration state'.
  $permissions['edit petition_espana_ registration state'] = array(
    'name' => 'edit petition_espana_ registration state',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'edit petition_french_cloned_ registration state'.
  $permissions['edit petition_french_cloned_ registration state'] = array(
    'name' => 'edit petition_french_cloned_ registration state',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'edit petition_german_ registration state'.
  $permissions['edit petition_german_ registration state'] = array(
    'name' => 'edit petition_german_ registration state',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'update any petition registration'.
  $permissions['update any petition registration'] = array(
    'name' => 'update any petition registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'update any petition_espana_ registration'.
  $permissions['update any petition_espana_ registration'] = array(
    'name' => 'update any petition_espana_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'update any petition_french_cloned_ registration'.
  $permissions['update any petition_french_cloned_ registration'] = array(
    'name' => 'update any petition_french_cloned_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'update any petition_german_ registration'.
  $permissions['update any petition_german_ registration'] = array(
    'name' => 'update any petition_german_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'update own petition registration'.
  $permissions['update own petition registration'] = array(
    'name' => 'update own petition registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'update own petition_espana_ registration'.
  $permissions['update own petition_espana_ registration'] = array(
    'name' => 'update own petition_espana_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'update own petition_french_cloned_ registration'.
  $permissions['update own petition_french_cloned_ registration'] = array(
    'name' => 'update own petition_french_cloned_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'update own petition_german_ registration'.
  $permissions['update own petition_german_ registration'] = array(
    'name' => 'update own petition_german_ registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'view own petition registration'.
  $permissions['view own petition registration'] = array(
    'name' => 'view own petition registration',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'view own petition_espana_ registration'.
  $permissions['view own petition_espana_ registration'] = array(
    'name' => 'view own petition_espana_ registration',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'view own petition_french_cloned_ registration'.
  $permissions['view own petition_french_cloned_ registration'] = array(
    'name' => 'view own petition_french_cloned_ registration',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'view own petition_german_ registration'.
  $permissions['view own petition_german_ registration'] = array(
    'name' => 'view own petition_german_ registration',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'view petition registration'.
  $permissions['view petition registration'] = array(
    'name' => 'view petition registration',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'view petition_espana_ registration'.
  $permissions['view petition_espana_ registration'] = array(
    'name' => 'view petition_espana_ registration',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'view petition_french_cloned_ registration'.
  $permissions['view petition_french_cloned_ registration'] = array(
    'name' => 'view petition_french_cloned_ registration',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'view petition_german_ registration'.
  $permissions['view petition_german_ registration'] = array(
    'name' => 'view petition_german_ registration',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  return $permissions;
}
