<?php
/**
 * @file
 * wise_petition.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function wise_petition_default_rules_configuration() {
  $items = array();
  $items['rules_redirect_after_petition_signing'] = entity_import('rules_config', '{ "rules_redirect_after_petition_signing" : {
      "LABEL" : "Redirect after petition signing",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "TAGS" : [ "registration" ],
      "REQUIRES" : [ "rules", "registration" ],
      "ON" : { "registration_insert" : [] },
      "DO" : [
        { "redirect" : { "url" : "campaign\\/signed-petitions", "force" : "0" } }
      ]
    }
  }');
  $items['rules_send_email_after_registration'] = entity_import('rules_config', '{ "rules_send_email_after_registration" : {
      "LABEL" : "Send email after petition signing",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "registration" ],
      "REQUIRES" : [ "rules", "registration" ],
      "ON" : { "registration_insert" : [] },
      "DO" : [
        { "mail" : {
            "to" : [ "registration:mail" ],
            "subject" : "Please confirm signing the Petition",
            "message" : "Dear sir \\/ madam,\\r\\n\\r\\nYou just signed our petition.\\r\\nTo complete you registration you have to reply to this email.\\r\\nYou don\\u0027t have to add anything to your reply, just send it!\\r\\n\\r\\nThanks for supporting the campaign Don\\u0027t Nuke the Climate.\\r\\n\\r\\nKind regards,\\r\\nWISE international",
            "from" : "info@wiseinternational.org",
            "language" : [ "" ]
          }
        },
        { "entity_fetch" : {
            "USING" : { "type" : "node", "id" : [ "registration:entity-id" ] },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
          }
        }
      ]
    }
  }');
  return $items;
}
