<?php
/**
 * @file
 * wise_virtual_march.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wise_virtual_march_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'bestsellers';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'commerce_order';
  $view->human_name = 'Virtual March latest donations';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Donated Virtual March items';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = '[type_1]';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Commerce Order: Referenced line items */
  $handler->display->display_options['relationships']['commerce_line_items_line_item_id']['id'] = 'commerce_line_items_line_item_id';
  $handler->display->display_options['relationships']['commerce_line_items_line_item_id']['table'] = 'field_data_commerce_line_items';
  $handler->display->display_options['relationships']['commerce_line_items_line_item_id']['field'] = 'commerce_line_items_line_item_id';
  $handler->display->display_options['relationships']['commerce_line_items_line_item_id']['required'] = TRUE;
  /* Relationship: Commerce Line item: Referenced products */
  $handler->display->display_options['relationships']['commerce_product_product_id']['id'] = 'commerce_product_product_id';
  $handler->display->display_options['relationships']['commerce_product_product_id']['table'] = 'field_data_commerce_product';
  $handler->display->display_options['relationships']['commerce_product_product_id']['field'] = 'commerce_product_product_id';
  $handler->display->display_options['relationships']['commerce_product_product_id']['relationship'] = 'commerce_line_items_line_item_id';
  $handler->display->display_options['relationships']['commerce_product_product_id']['required'] = TRUE;
  /* Relationship: Commerce Product: Referencing Node */
  $handler->display->display_options['relationships']['field_product']['id'] = 'field_product';
  $handler->display->display_options['relationships']['field_product']['table'] = 'commerce_product';
  $handler->display->display_options['relationships']['field_product']['field'] = 'field_product';
  $handler->display->display_options['relationships']['field_product']['relationship'] = 'commerce_product_product_id';
  $handler->display->display_options['relationships']['field_product']['label'] = 'Node';
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['relationship'] = 'field_product';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'view details';
  /* Field: Commerce Line item: Your personal slogan */
  $handler->display->display_options['fields']['field_banner_slogan']['id'] = 'field_banner_slogan';
  $handler->display->display_options['fields']['field_banner_slogan']['table'] = 'field_data_field_banner_slogan';
  $handler->display->display_options['fields']['field_banner_slogan']['field'] = 'field_banner_slogan';
  $handler->display->display_options['fields']['field_banner_slogan']['relationship'] = 'commerce_line_items_line_item_id';
  $handler->display->display_options['fields']['field_banner_slogan']['label'] = '';
  $handler->display->display_options['fields']['field_banner_slogan']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slogan']['hide_empty'] = TRUE;
  /* Field: Commerce Line item: Your personal slogan */
  $handler->display->display_options['fields']['field_your_personal_slogan']['id'] = 'field_your_personal_slogan';
  $handler->display->display_options['fields']['field_your_personal_slogan']['table'] = 'field_data_field_your_personal_slogan';
  $handler->display->display_options['fields']['field_your_personal_slogan']['field'] = 'field_your_personal_slogan';
  $handler->display->display_options['fields']['field_your_personal_slogan']['relationship'] = 'commerce_line_items_line_item_id';
  $handler->display->display_options['fields']['field_your_personal_slogan']['label'] = '';
  $handler->display->display_options['fields']['field_your_personal_slogan']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_your_personal_slogan']['hide_empty'] = TRUE;
  /* Field: Commerce Line item: Your name */
  $handler->display->display_options['fields']['field_your_name']['id'] = 'field_your_name';
  $handler->display->display_options['fields']['field_your_name']['table'] = 'field_data_field_your_name';
  $handler->display->display_options['fields']['field_your_name']['field'] = 'field_your_name';
  $handler->display->display_options['fields']['field_your_name']['relationship'] = 'commerce_line_items_line_item_id';
  $handler->display->display_options['fields']['field_your_name']['label'] = '';
  $handler->display->display_options['fields']['field_your_name']['element_label_colon'] = FALSE;
  /* Field: Content: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'node';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['relationship'] = 'field_product';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['type']['machine_name'] = TRUE;
  /* Field: Commerce Line Item: Type */
  $handler->display->display_options['fields']['type_1']['id'] = 'type_1';
  $handler->display->display_options['fields']['type_1']['table'] = 'commerce_line_item';
  $handler->display->display_options['fields']['type_1']['field'] = 'type';
  $handler->display->display_options['fields']['type_1']['relationship'] = 'commerce_line_items_line_item_id';
  $handler->display->display_options['fields']['type_1']['label'] = '';
  $handler->display->display_options['fields']['type_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['type_1']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['type_1']['element_label_colon'] = FALSE;
  /* Field: Commerce Line item: Define a donation amount */
  $handler->display->display_options['fields']['field_donation_amount_small']['id'] = 'field_donation_amount_small';
  $handler->display->display_options['fields']['field_donation_amount_small']['table'] = 'field_data_field_donation_amount_small';
  $handler->display->display_options['fields']['field_donation_amount_small']['field'] = 'field_donation_amount_small';
  $handler->display->display_options['fields']['field_donation_amount_small']['relationship'] = 'commerce_line_items_line_item_id';
  $handler->display->display_options['fields']['field_donation_amount_small']['label'] = '';
  $handler->display->display_options['fields']['field_donation_amount_small']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_donation_amount_small']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_donation_amount_small']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Sort criterion: COUNT(Commerce Product: Title) */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'commerce_product';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['sorts']['title']['relationship'] = 'commerce_product_product_id';
  $handler->display->display_options['sorts']['title']['group_type'] = 'count';
  $handler->display->display_options['sorts']['title']['order'] = 'DESC';
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: Commerce Line Item: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'commerce_line_item';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['relationship'] = 'commerce_line_items_line_item_id';
  $handler->display->display_options['filters']['type']['value'] = array(
    'virtual_march_flag' => 'virtual_march_flag',
    'virtual_march_large' => 'virtual_march_large',
    'virtual_march_small' => 'virtual_march_small',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Commerce Product: Type */
  $handler->display->display_options['filters']['type_1']['id'] = 'type_1';
  $handler->display->display_options['filters']['type_1']['table'] = 'commerce_product';
  $handler->display->display_options['filters']['type_1']['field'] = 'type';
  $handler->display->display_options['filters']['type_1']['relationship'] = 'commerce_product_product_id';
  $handler->display->display_options['filters']['type_1']['value'] = array(
    'donation' => 'donation',
    'product' => 'product',
  );
  $handler->display->display_options['filters']['type_1']['group'] = 1;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['footer'] = FALSE;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['label'] = 'Link to page';
  $handler->display->display_options['footer']['area']['content'] = '<p>Your banner here? <a class="button" href="campaign/cop-21-paris">join!</a></p>';
  $handler->display->display_options['footer']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Commerce Line Item: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'commerce_line_item';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['relationship'] = 'commerce_line_items_line_item_id';
  $handler->display->display_options['filters']['type']['value'] = array(
    'virtual_march_flag' => 'virtual_march_flag',
    'virtual_march_large' => 'virtual_march_large',
    'virtual_march_small' => 'virtual_march_small',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Commerce Product: Type */
  $handler->display->display_options['filters']['type_1']['id'] = 'type_1';
  $handler->display->display_options['filters']['type_1']['table'] = 'commerce_product';
  $handler->display->display_options['filters']['type_1']['field'] = 'type';
  $handler->display->display_options['filters']['type_1']['relationship'] = 'commerce_product_product_id';
  $handler->display->display_options['filters']['type_1']['value'] = array(
    'donation' => 'donation',
    'product' => 'product',
  );
  $handler->display->display_options['filters']['type_1']['group'] = 1;
  /* Filter criterion: Commerce Order: Order status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'commerce_order';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = array(
    'checkout_checkout' => 'checkout_checkout',
    'checkout_shipping' => 'checkout_shipping',
    'checkout_complete' => 'checkout_complete',
    'pending' => 'pending',
    'completed' => 'completed',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'test-page';
  $export['bestsellers'] = $view;

  $view = new view();
  $view->name = 'virtual_march_';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Virtual March ';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Virtual March ';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'full';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['sorts']['title']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'virtual_march_donation_large' => 'virtual_march_donation_large',
    'virtual_march_donation_small' => 'virtual_march_donation_small',
    'virtual_march_donation_flag' => 'virtual_march_donation_flag',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status_1']['id'] = 'status_1';
  $handler->display->display_options['filters']['status_1']['table'] = 'node';
  $handler->display->display_options['filters']['status_1']['field'] = 'status';
  $handler->display->display_options['filters']['status_1']['value'] = '1';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['virtual_march_'] = $view;

  return $export;
}
