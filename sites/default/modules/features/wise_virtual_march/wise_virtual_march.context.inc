<?php
/**
 * @file
 * wise_virtual_march.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_virtual_march_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'join_virtual_march';
  $context->description = '';
  $context->tag = 'Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'campaign/cop-21-paris' => 'campaign/cop-21-paris',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-virtual_march_-block' => array(
          'module' => 'views',
          'delta' => 'virtual_march_-block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Page');
  $export['join_virtual_march'] = $context;

  return $export;
}
