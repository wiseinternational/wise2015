<?php
/**
 * @file
 * wise_virtual_march.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function wise_virtual_march_default_rules_configuration() {
  $items = array();
  $items['rules_add_virtual_march_donation_to_price'] = entity_import('rules_config', '{ "rules_add_virtual_march_donation_to_price" : {
      "LABEL" : "Add Virtual March donation to price Small",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_line_item", "commerce_product_reference", "entity" ],
      "ON" : {
        "commerce_product_calculate_sell_price" : [],
        "commerce_line_item_presave" : []
      },
      "IF" : [
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item" ],
            "field" : "field_donation_amount_small"
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "commerce-line-item:field-donation-amount-small" ],
              "op" : "*",
              "input_2" : "100"
            },
            "PROVIDE" : { "result" : { "donation_amount_in_euro" : "Donation amount in euro" } }
          }
        },
        { "commerce_line_item_unit_price_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "donation-amount-in-euro" ],
            "component_name" : "base_price",
            "round_mode" : "0"
          }
        }
      ]
    }
  }');
  $items['rules_add_virtual_march_donation_to_price_flag'] = entity_import('rules_config', '{ "rules_add_virtual_march_donation_to_price_flag" : {
      "LABEL" : "Add Virtual March donation to price Flag",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_line_item", "commerce_product_reference", "entity" ],
      "ON" : {
        "commerce_product_calculate_sell_price" : [],
        "commerce_line_item_presave" : []
      },
      "IF" : [
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item" ],
            "field" : "field_donation_amount_flag"
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "commerce-line-item:field-donation-amount-flag" ],
              "op" : "*",
              "input_2" : "100"
            },
            "PROVIDE" : { "result" : { "donation_amount_in_euro" : "Donation amount in euro" } }
          }
        },
        { "commerce_line_item_unit_price_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "donation-amount-in-euro" ],
            "component_name" : "base_price",
            "round_mode" : "0"
          }
        }
      ]
    }
  }');
  $items['rules_add_virtual_march_donation_to_price_large'] = entity_import('rules_config', '{ "rules_add_virtual_march_donation_to_price_large" : {
      "LABEL" : "Add Virtual March donation to price Large",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_line_item", "commerce_product_reference", "entity" ],
      "ON" : {
        "commerce_product_calculate_sell_price" : [],
        "commerce_line_item_presave" : []
      },
      "IF" : [
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item" ],
            "field" : "field_donation_amount_large"
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "commerce-line-item:field-donation-amount-large" ],
              "op" : "*",
              "input_2" : "100"
            },
            "PROVIDE" : { "result" : { "donation_amount_large_in_euro" : "Donation amount large in euro" } }
          }
        },
        { "commerce_line_item_unit_price_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "donation-amount-large-in-euro" ],
            "component_name" : "base_price",
            "round_mode" : "0"
          }
        }
      ]
    }
  }');
  return $items;
}
