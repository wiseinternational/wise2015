<?php
/**
 * @file
 * wise_virtual_march.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wise_virtual_march_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create virtual_march_donation_flag content'.
  $permissions['create virtual_march_donation_flag content'] = array(
    'name' => 'create virtual_march_donation_flag content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create virtual_march_donation_large content'.
  $permissions['create virtual_march_donation_large content'] = array(
    'name' => 'create virtual_march_donation_large content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create virtual_march_donation_small content'.
  $permissions['create virtual_march_donation_small content'] = array(
    'name' => 'create virtual_march_donation_small content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any virtual_march_donation_flag content'.
  $permissions['delete any virtual_march_donation_flag content'] = array(
    'name' => 'delete any virtual_march_donation_flag content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any virtual_march_donation_large content'.
  $permissions['delete any virtual_march_donation_large content'] = array(
    'name' => 'delete any virtual_march_donation_large content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any virtual_march_donation_small content'.
  $permissions['delete any virtual_march_donation_small content'] = array(
    'name' => 'delete any virtual_march_donation_small content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own virtual_march_donation_flag content'.
  $permissions['delete own virtual_march_donation_flag content'] = array(
    'name' => 'delete own virtual_march_donation_flag content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own virtual_march_donation_large content'.
  $permissions['delete own virtual_march_donation_large content'] = array(
    'name' => 'delete own virtual_march_donation_large content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own virtual_march_donation_small content'.
  $permissions['delete own virtual_march_donation_small content'] = array(
    'name' => 'delete own virtual_march_donation_small content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any virtual_march_donation_flag content'.
  $permissions['edit any virtual_march_donation_flag content'] = array(
    'name' => 'edit any virtual_march_donation_flag content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any virtual_march_donation_large content'.
  $permissions['edit any virtual_march_donation_large content'] = array(
    'name' => 'edit any virtual_march_donation_large content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any virtual_march_donation_small content'.
  $permissions['edit any virtual_march_donation_small content'] = array(
    'name' => 'edit any virtual_march_donation_small content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own virtual_march_donation_flag content'.
  $permissions['edit own virtual_march_donation_flag content'] = array(
    'name' => 'edit own virtual_march_donation_flag content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own virtual_march_donation_large content'.
  $permissions['edit own virtual_march_donation_large content'] = array(
    'name' => 'edit own virtual_march_donation_large content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own virtual_march_donation_small content'.
  $permissions['edit own virtual_march_donation_small content'] = array(
    'name' => 'edit own virtual_march_donation_small content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
