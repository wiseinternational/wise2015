<?php
/**
 * @file
 * wise_virtual_march.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_virtual_march_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wise_virtual_march_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wise_virtual_march_node_info() {
  $items = array(
    'virtual_march_donation_flag' => array(
      'name' => t('Virtual March donation medium'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'virtual_march_donation_large' => array(
      'name' => t('Virtual March donation Large'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'virtual_march_donation_small' => array(
      'name' => t('Virtual March donation Small'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
