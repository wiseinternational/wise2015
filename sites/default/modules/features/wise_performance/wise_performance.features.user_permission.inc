<?php
/**
 * @file
 * wise_performance.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wise_performance_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'boost flush pages'.
  $permissions['boost flush pages'] = array(
    'name' => 'boost flush pages',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'boost',
  );

  return $permissions;
}
