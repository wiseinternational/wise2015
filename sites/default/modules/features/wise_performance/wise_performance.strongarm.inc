<?php
/**
 * @file
 * wise_performance.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function wise_performance_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_cacheability_option';
  $strongarm->value = '0';
  $export['boost_cacheability_option'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_cacheability_pages';
  $strongarm->value = 'cart
webshop
webshop/*
product/*';
  $export['boost_cacheability_pages'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_enabled_application/json';
  $strongarm->value = FALSE;
  $export['boost_enabled_application/json'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_enabled_application/rss';
  $strongarm->value = 0;
  $export['boost_enabled_application/rss'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_enabled_application/rss+xml';
  $strongarm->value = 0;
  $export['boost_enabled_application/rss+xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_enabled_application/xml';
  $strongarm->value = 0;
  $export['boost_enabled_application/xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_enabled_image/jpeg';
  $strongarm->value = FALSE;
  $export['boost_enabled_image/jpeg'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_enabled_image/png';
  $strongarm->value = FALSE;
  $export['boost_enabled_image/png'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_enabled_text/html';
  $strongarm->value = 1;
  $export['boost_enabled_text/html'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_enabled_text/javascript';
  $strongarm->value = 0;
  $export['boost_enabled_text/javascript'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_enabled_text/xml';
  $strongarm->value = 0;
  $export['boost_enabled_text/xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_extension_application/rss';
  $strongarm->value = 'xml';
  $export['boost_extension_application/rss'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_extension_application/rss+xml';
  $strongarm->value = 'xml';
  $export['boost_extension_application/rss+xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_extension_application/xml';
  $strongarm->value = 'xml';
  $export['boost_extension_application/xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_extension_text/html';
  $strongarm->value = 'html';
  $export['boost_extension_text/html'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_extension_text/javascript';
  $strongarm->value = 'json';
  $export['boost_extension_text/javascript'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_extension_text/xml';
  $strongarm->value = 'xml';
  $export['boost_extension_text/xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_gzip_application/rss';
  $strongarm->value = 1;
  $export['boost_gzip_application/rss'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_gzip_application/rss+xml';
  $strongarm->value = 1;
  $export['boost_gzip_application/rss+xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_gzip_application/xml';
  $strongarm->value = 1;
  $export['boost_gzip_application/xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_gzip_text/html';
  $strongarm->value = 1;
  $export['boost_gzip_text/html'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_gzip_text/javascript';
  $strongarm->value = 1;
  $export['boost_gzip_text/javascript'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_gzip_text/xml';
  $strongarm->value = 1;
  $export['boost_gzip_text/xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_max_application/rss';
  $strongarm->value = '3600';
  $export['boost_lifetime_max_application/rss'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_max_application/rss+xml';
  $strongarm->value = '3600';
  $export['boost_lifetime_max_application/rss+xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_max_application/xml';
  $strongarm->value = '3600';
  $export['boost_lifetime_max_application/xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_max_text/html';
  $strongarm->value = '3600';
  $export['boost_lifetime_max_text/html'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_max_text/javascript';
  $strongarm->value = '3600';
  $export['boost_lifetime_max_text/javascript'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_max_text/xml';
  $strongarm->value = '3600';
  $export['boost_lifetime_max_text/xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_min_application/rss';
  $strongarm->value = '0';
  $export['boost_lifetime_min_application/rss'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_min_application/rss+xml';
  $strongarm->value = '0';
  $export['boost_lifetime_min_application/rss+xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_min_application/xml';
  $strongarm->value = '0';
  $export['boost_lifetime_min_application/xml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_min_text/html';
  $strongarm->value = '0';
  $export['boost_lifetime_min_text/html'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_min_text/javascript';
  $strongarm->value = '0';
  $export['boost_lifetime_min_text/javascript'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'boost_lifetime_min_text/xml';
  $strongarm->value = '0';
  $export['boost_lifetime_min_text/xml'] = $strongarm;

  return $export;
}
