<?php
/**
 * @file
 * wise_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_content_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wise_content_types_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Gebruik <em>artikelen</em> voor tijdsgebonden inhoud zoals nieuws, persberichten of blog-berichten.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'nodeblock_algemeen' => array(
      'name' => t('Content block'),
      'base' => 'node_content',
      'description' => t('Content dat in een blok getoond moet worden.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('Content type voor statische pagina\'s die vaak aan een menu gekoppeld zijn.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
